declare module es {
    class Ref<T> {
        value: T;
        constructor(value: T);
    }
}
declare module es {
    class Common {
        /**
         * 浅拷贝
         * @param src 拷贝对象
         * @param dest 覆盖对象
         */
        static Clone(src: any, dest: any): void;
    }
}
declare module es {
    class Debug {
        /** 是否在debug模式 */
        static debug: boolean;
        /**
         * 打印
         * @param content 文本内容
         * @param fontColor 字体颜色
         * @param bgColor 背景颜色
         */
        static log(content: any, fontColor?: string, bgColor?: string): void;
        /**
         * 打印报错
         * @param content 文本内容
         */
        static error(content: any): void;
        /**
         * DEBUG模式下 打印日志的内容派发
         */
        static readonly EVENT_DEBUG_LOG: string;
        protected static eventLog(content: any): boolean;
        /**
         * 获取当前平台信息
         */
        static getPlatformInfo(): {
            /** 当前浏览器 user-Agent 字符串*/
            userAgent: string;
            /** 当前运行在安卓平台上 */
            onAndroid: boolean;
            /** 当前运行在苹果平台上 */
            onIOS: boolean;
            /** 当前运行在Ipad平台上 */
            onIpad: boolean;
            /** 当前运行在微信平台上 */
            onWechat: boolean;
        };
        private static _gfx;
        private static readonly gfx;
        /**
         * 绘制debug线框
         * @param star 开始 global pos
         * @param end 结束
         * @param color 颜色
         */
        static DrawLine(star: {
            x: number;
            y: number;
        }, end: {
            x: number;
            y: number;
        }, color?: string): void;
        /**
         * 绘制边框
         * @param posOrRect  global pos
         * @param sizeOrColor
         * @param color
         */
        static DrawRectangles(posOrRect: {
            x: number;
            y: number;
        } | {
            x: number;
            y: number;
            width: number;
            height: number;
        }, sizeOrColor: {
            x: number;
            y: number;
        } | string, color?: string): void;
        /**
         * 绘制圆框
         * @param pos
         * @param r
         * @param color
         */
        static DrawCircle(pos: {
            x: number;
            y: number;
        }, r: number, color?: string): void;
        /**
         * 绘制扇形
         * @param pos 位置
         * @param r 半径
         * @param startAngle 开始角度
         * @param endAngle end角度
         * @param color 颜色
         */
        static DrawPie(pos: {
            x: number;
            y: number;
        }, r: number, startAngle: number, endAngle: number, color?: string): void;
        /**
         * 绘制文字
         * @param text 内容
         * @param pos 位置
         * @param color 字体颜色
         * @param fontSize 字体像素大小
         */
        static GUILabel(text: string, pos: {
            x: number;
            y: number;
        }, color?: string, fontSize?: number): void;
    }
}
declare module es {
    enum Edge {
        top = 0,
        bottom = 1,
        left = 2,
        right = 3
    }
}
declare module es {
    class Functor {
        protected fn: Function;
        protected caller: any;
        protected constructor(fn: Function, caller: any);
        /**@static 创建一个仿函数 */
        static New(fn: Function, caller: any): Functor;
        /**@static 判断两个仿函数是否相等 */
        static Equal(me: Functor, he: Functor): boolean;
        /** 通过调用者 和 方法来判断是否相等 */
        equal2(caller: any, fn: Function): boolean;
        /** 判断两个仿函数是否相等 */
        equal(he: Functor): boolean;
        /** 获取调用者 */
        getCaller(): any;
        /** 获取方法 */
        getMethod(): Function;
        /** 调用 */
        invoke(): any;
        /** 带参数调用 */
        invokeArgs(...args: any[]): any;
        /** 重写toString */
        toString(): string;
    }
}
declare module es {
    class Hash {
        /**
         * 从一个字节数组中计算一个哈希值
         * @param data
         */
        static computeHash(...data: number[]): number;
    }
}
declare module es {
    /**
     * 实现该接口用于判定两个对象是否相等的快速接口
     */
    interface IEquatable<T> {
        equals(other: T): boolean;
    }
}
declare module es {
    enum CharType {
        Chinese = 0,
        Number = 1,
        Lowercase = 2,
        Uppercase = 3,
        Symbol = 4
    }
    class StringTool {
        /**
    * 字符检测
    * @param _char 待判断的字符
    * @returns
    */
        static charTest(_char: any): CharType;
        /**
         * 返回全大写
         * @param str
         * @return
         */
        static toUpCase(str: string): string;
        /**
         * 返回全小写
         * @param str
         * @return
         */
        static toLowCase(str: string): string;
        /**
         * 返回首字母大写
         * @param str
         * @return
         */
        static toUpHead(str: string): string;
        /**
         * 返回首字母小写
         * @param str
         * @return
         */
        static toLowHead(str: string): string;
        /**
         * 包名转路径名
         * @param packageName
         * @return
         */
        static packageToFolderPath(packageName: string): string;
        static insert(str: string, iStr: string, index: number): string;
        static insertAfter(str: string, iStr: string, tarStr: string, isLast?: boolean): string;
        static insertBefore(str: string, iStr: string, tarStr: string, isLast?: boolean): string;
        static insertParamToFun(funStr: string, params: any[]): string;
        /**
         * 去除空格和换行符
         * @param str
         * @return
         */
        static trim(str: string, vList?: any[]): string;
        static emptyStrDic: any;
        static isEmpty(str: string): boolean;
        static trimLeft(str: string): string;
        static trimRight(str: string): string;
        static trimSide(str: string): string;
        static specialChars: any;
        static isOkFileName(fileName: string): boolean;
        static trimButEmpty(str: string): string;
        static removeEmptyStr(strArr: any[]): any[];
        static ifNoAddToTail(str: string, sign: string): string;
        static trimEmptyLine(str: string): string;
        static isEmptyLine(str: string): boolean;
        static removeCommentLine(lines: any[]): any[];
        static addIfNotEmpty(arr: any[], str: string): void;
        static trimExt(str: string, vars: any[]): string;
        static getBetween(str: string, left: string, right: string, ifMax?: boolean): string;
        static getSplitLine(line: string, split?: string): any[];
        static getLeft(str: string, sign: string): string;
        static getRight(str: string, sign: string): string;
        static delelteItem(arr: any[]): void;
        static getWords(line: string): any[];
        static getLinesI(startLine: number, endLine: number, lines: any[]): any[];
        static structfy(str: string, inWidth?: number, removeEmpty?: boolean): string;
        static emptyDic: any;
        static getEmptyStr(width: number): string;
        static getPariCount(str: string, inChar?: string, outChar?: string): number;
        static readInt(str: string, startI?: number): number;
        /**
         * 替换文本
         * @param str
         * @param oStr
         * @param nStr
         * @return
         */
        static getReplace(str: string, oStr: string, nStr: string): string;
        static getWordCount(str: string, findWord: string): number;
        static getResolvePath(path: string, basePath: string): string;
        static isAbsPath(path: string): boolean;
        static removeLastSign(str: string, sign: string): string;
        static getParamArr(str: string): any[];
        static copyStr(str: string): string;
        /**
         * 将Array转成字符串
         * @param arr
         * @return
         */
        static ArrayToString(arr: any[]): string;
        static getArrayItems(arr: any[]): string;
        static parseItem(item: any): string;
        static alphaSigns: any;
        static initAlphaSign(): void;
        static addSign(ss: string, e: string, tar: any): void;
        static isPureAlphaNum(str: string): boolean;
        static format(text: any, ...args: any[]): any;
        /**
         * 数字位数不足 前面补0
         * @param num 数值
         * @param len 例如 3 =>   ...008,009,010,011
         * @returns
         */
        static formatZero(num: any, len: any): any;
    }
}
declare module es {
    /**
     * 类型检验工具类
     */
    class TypeUtils {
        /**
         * 检测是否是字符串类型，注意new String检测不通过的
         */
        static isString(obj: any): boolean;
        /**
         * 检测是不是数组
         */
        static isArray(obj: any): boolean;
        /**
         * 检测是不是数字，注意new Number不算进来
         */
        static isNumber(obj: any): boolean;
        /**
         * 是不是对象，数组也是一种对象
         */
        static isObject(obj: any): boolean;
        /**
         * 是不是函数
         */
        static isFunction(obj: any): boolean;
        /**
         * 检测类型是否相等
         */
        static isSame(obj0: any, obj1: any): boolean;
    }
}
declare module es {
    class EasySave {
        /**
         * 存入一个对象
         * @param key
         * @param obj
         */
        static setObj(key: string, obj: string | object): void;
        /**
         * 获取一个对象
         * @param key
         * @returns null | T
         */
        static getObj<T>(key: string): T;
        /**
         * 存储Cookie
         * @param k 字符串
         * @param v 字符串
         * @param exdays 数据有效日期 默认30天, 仅Cookie有效
         */
        static set(k: string, v: string, exdays?: number): void;
        /**
         * 获取Cookie
         * @param k 字符串
         * @returns value | null
         */
        static get(k: string): string | null;
        /**
         * 判断cookie是否存在
         * @param k
         * @returns true | false
         */
        static has(k: string): boolean;
        /**
         * 删除cookie
         * @param k
         */
        static del(k: string): void;
        /**
         * 清理所有Cookie
         */
        static clear(): void;
    }
}
declare module es {
    class MathHelper {
        static readonly Epsilon: number;
        static readonly Rad2Deg: number;
        static readonly Deg2Rad: number;
        /**
         * 表示pi除以2的值(1.57079637)
         */
        static readonly PiOver2: number;
        /**
         * 将弧度转换成角度。
         * @param radians 用弧度表示的角
         */
        static toDegrees(radians: number): number;
        /**
         * 将角度转换为弧度
         * @param degrees
         */
        static toRadians(degrees: number): number;
        /**
         * mapps值(在leftMin - leftMax范围内)到rightMin - rightMax范围内的值
         * @param value
         * @param leftMin
         * @param leftMax
         * @param rightMin
         * @param rightMax
         */
        static map(value: number, leftMin: number, leftMax: number, rightMin: number, rightMax: number): number;
        static lerp(value1: number, value2: number, amount: number): number;
        static clamp(value: number, min: number, max: number): number;
        /**
         * 给定圆心、半径和角度，得到圆周上的一个点。0度是3点钟。
         * @param circleCenter
         * @param radius
         * @param angleInDegrees
         */
        static pointOnCirlce(circleCenter: Vector2, radius: number, angleInDegrees: number): Vector2;
        /**
         * 如果值为偶数，返回true
         * @param value
         */
        static isEven(value: number): boolean;
        /**
         * 数值限定在0-1之间
         * @param value
         */
        static clamp01(value: number): number;
        static angleBetweenVectors(from: Vector2, to: Vector2): number;
        static angleToVector(angleRadians: number, length: number): Vector2;
        /**
         * 增加t并确保它总是大于或等于0并且小于长度
         * @param t
         * @param length
         */
        static incrementWithWrap(t: number, length: number): number;
        /**
         * 以roundToNearest为步长，将值舍入到最接近的数字。例如：在125中找到127到最近的5个结果
         * @param value
         * @param roundToNearest
         */
        static roundToNearest(value: number, roundToNearest: number): number;
        /**
         * 检查传递的值是否在某个阈值之下。对于小规模、精确的比较很有用
         * @param value
         * @param ep
         */
        static withinEpsilon(value: number, ep?: number): boolean;
        /**
         * 由上移量向上移。start可以小于或大于end。例如:开始是2，结束是10，移位是4，结果是6
         * @param start
         * @param end
         * @param shift
         */
        static approach(start: number, end: number, shift: number): number;
        /**
         * 计算两个给定角之间的最短差值（度数）
         * @param current
         * @param target
         */
        static deltaAngle(current: number, target: number): number;
        /**
         * 循环t，使其永远不大于长度，永远不小于0
         * @param t
         * @param length
         */
        static repeat(t: number, length: number): number;
    }
}
declare module es {
    /**
     * @property {number} EPSILON
     */
    const EPSILON = 0.000001;
    const INT_BITS = 32;
    const INT_MAX = 2147483647;
    const INT_MIN: number;
    class Mathf {
        /**
 * Tests whether or not the arguments have approximately the same value, within an absolute
 * or relative tolerance of glMatrix.EPSILON (an absolute tolerance is used for values less
 * than or equal to 1.0, and a relative tolerance is used for larger values)
 *
 * @param {Number} a The first number to test.
 * @param {Number} b The second number to test.
 * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
 */
        static equals(a: any, b: any): boolean;
        /**
         * 插值
         * @param from
         * @param to
         * @param ratio
         * @returns
         */
        static lerp(from: any, to: any, ratio: any): any;
        /**
         * 混合
         * @param src
         * @param dest
         * @param ration
         */
        static mix(src: any, dest: any, ration: any): any;
        /**
         * Returns a floating-point random number between min (inclusive) and max (exclusive).
         *
         * @method randomRange
         * @param {number} min
         * @param {number} max
         * @return {number} the random number
         */
        static randomRange(min: any, max: any): any;
        /**
        * Returns a random integer between min (inclusive) and max (exclusive).
        *
        * @method randomRangeInt
        * @param {number} min
        * @param {number} max
        * @return {number} the random integer
        */
        static randomRangeInt(min: any, max: any): number;
        /**
         * Linear congruential generator using Hull-Dobell Theorem.
         *
         * @method pseudoRandom
         * @param {number} seed the random seed
         * @return {number} the pseudo random
         */
        static pseudoRandom(seed: any): number;
        /**
         * Returns a floating-point pseudo-random number between min (inclusive) and max (exclusive).
         *
         * @method pseudoRandomRange
         * @param {number} seed
         * @param {number} min
         * @param {number} max
         * @return {number} the random number
         */
        static pseudoRandomRange(seed: any, min: any, max: any): any;
        /**
 * Returns a pseudo-random integer between min (inclusive) and max (exclusive).
 *
 * @method pseudoRandomRangeInt
 * @param {number} seed
 * @param {number} min
 * @param {number} max
 * @return {number} the random integer
 */
        static pseudoRandomRangeInt(seed: any, min: any, max: any): number;
        /**
 * Returns the next power of two for the value
 *
 * @method nextPow2
 * @param {number} val
 * @return {number} the the next power of two
 */
        static nextPow2(val: any): any;
        /**
         * Returns ratio of a value within a given range
         *
         * @method repeat
         * @param {number} from start value
         * @param {number} to end value
         * @param {number} value given value
         * @return {number} the ratio between [from,to]
         */
        static inverseLerp(from: any, to: any, value: any): number;
        /**
         * Returns -1, 0, +1 depending on sign of x.
         *
         * @method sign
         * @param {number} v
         */
        static sign(v: any): number;
        /**
 * Clamps a value between a minimum float and maximum float value.
 *
 * @method clamp
 * @param {number} val
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
        static clamp(val: any, min: any, max: any): any;
        /**
         * Tests whether or not the arguments have approximately the same value by given maxDiff
         *
         * @param {Number} a The first number to test.
         * @param {Number} b The second number to test.
         * @param {Number} maxDiff Maximum difference.
         * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
         */
        static approx(a: any, b: any, maxDiff: any): boolean;
        /**
        * Convert Degree To Radian
        *
        * @param {Number} a Angle in Degrees
        */
        static toRadian(a: any): number;
        /**
* Convert Radian To Degree
*
* @param {Number} a Angle in Radian
*/
        static toDegree(a: any): number;
        /**
         * Determine whether an integer n is a power of 2
         * @param n
         * @returns
         */
        static isPow(n: any): boolean;
        /**
         * Gets the unique, non-repeating value of an integer array
         * @param arr
         * @returns
         */
        static findChosenOne(arr: number[], i?: number, res?: number): any;
    }
}
declare module es {
    /**
     * 代表右手4x4浮点矩阵，可以存储平移、比例和旋转信息
     */
    class Matrix {
        m11: number;
        m12: number;
        m13: number;
        m14: number;
        m21: number;
        m22: number;
        m23: number;
        m24: number;
        m31: number;
        m32: number;
        m33: number;
        m34: number;
        m41: number;
        m42: number;
        m43: number;
        m44: number;
        /**
         * 为自定义的正交视图创建一个新的投影矩阵
         * @param left
         * @param right
         * @param top
         * @param zFarPlane
         * @param result
         */
        static createOrthographicOffCenter(left: number, right: number, bottom: number, top: number, zNearPlane: number, zFarPlane: number, result?: Matrix): void;
        /**
         * 创建一个新的矩阵，其中包含两个矩阵的乘法。
         * @param matrix1
         * @param matrix2
         * @param result
         */
        static multiply(matrix1: Matrix, matrix2: Matrix, result?: Matrix): void;
    }
}
declare module es {
    /**
     * 表示右手3 * 3的浮点矩阵，可以存储平移、缩放和旋转信息。
     */
    class Matrix2D implements IEquatable<Matrix2D> {
        m11: number;
        m12: number;
        m21: number;
        m22: number;
        m31: number;
        m32: number;
        /**
         * 返回标识矩阵
         */
        static readonly identity: Matrix2D;
        /**
         * 储存在该矩阵中的位置
         */
        translation: Vector2;
        /**
         * 以弧度为单位的旋转，存储在这个矩阵中
         */
        rotation: number;
        /**
         * 矩阵中存储的旋转度数
         */
        rotationDegrees: number;
        /**
         * 储存在这个矩阵中的缩放
         */
        scale: Vector2;
        /**
         * 构建一个矩阵
         * @param m11
         * @param m12
         * @param m21
         * @param m22
         * @param m31
         * @param m32
         */
        constructor(m11: number, m12: number, m21: number, m22: number, m31: number, m32: number);
        /**
         * 创建一个新的围绕Z轴的旋转矩阵2D
         * @param radians
         */
        static createRotation(radians: number): Matrix2D;
        /**
         * 创建一个新的缩放矩阵2D
         * @param xScale
         * @param yScale
         */
        static createScale(xScale: number, yScale: number): Matrix2D;
        /**
         * 创建一个新的平移矩阵2D
         * @param xPosition
         * @param yPosition
         */
        static createTranslation(xPosition: number, yPosition: number): Matrix2D;
        static invert(matrix: Matrix2D): Matrix2D;
        /**
         * 创建一个新的matrix, 它包含两个矩阵的和。
         * @param matrix
         */
        add(matrix: Matrix2D): Matrix2D;
        substract(matrix: Matrix2D): Matrix2D;
        divide(matrix: Matrix2D): Matrix2D;
        multiply(matrix: Matrix2D): Matrix2D;
        determinant(): number;
        /**
         * 创建一个新的Matrix2D，包含指定矩阵中的线性插值。
         * @param matrix1
         * @param matrix2
         * @param amount
         */
        static lerp(matrix1: Matrix2D, matrix2: Matrix2D, amount: number): Matrix2D;
        /**
         * 交换矩阵的行和列
         * @param matrix
         */
        static transpose(matrix: Matrix2D): Matrix2D;
        mutiplyTranslation(x: number, y: number): Matrix2D;
        /**
         * 比较当前实例是否等于指定的Matrix2D
         * @param other
         */
        equals(other: Matrix2D): boolean;
        static toMatrix(mat: Matrix2D): Matrix;
        toString(): string;
    }
}
declare module es {
    class MatrixHelper {
        /**
         * 创建一个新的Matrix2D，其中包含两个矩阵的和
         * @param matrix1
         * @param matrix2
         */
        static add(matrix1: Matrix2D, matrix2: Matrix2D): Matrix2D;
        /**
         * 将一个Matrix2D的元素除以另一个矩阵的元素
         * @param matrix1
         * @param matrix2
         */
        static divide(matrix1: Matrix2D, matrix2: Matrix2D): Matrix2D;
        /**
         * 创建一个新的Matrix2D，包含两个矩阵的乘法
         * @param matrix1
         * @param matrix2
         */
        static mutiply(matrix1: Matrix2D, matrix2: Matrix2D | number): Matrix2D;
        /**
         * 创建一个新的Matrix2D，包含一个矩阵与另一个矩阵的减法。
         * @param matrix1
         * @param matrix2
         */
        static subtract(matrix1: Matrix2D, matrix2: Matrix2D): Matrix2D;
    }
}
declare module es {
    class Rectangle implements IEquatable<Rectangle> {
        /**
         * 该矩形的左上角的x坐标
         */
        x: number;
        /**
         * 该矩形的左上角的y坐标
         */
        y: number;
        /**
         * 该矩形的宽度
         */
        width: number;
        /**
         * 该矩形的高度
         */
        height: number;
        /**
         * 返回X=0, Y=0, Width=0, Height=0的矩形
         */
        static readonly empty: Rectangle;
        /**
         * 返回一个Number.Min/Max值的矩形
         */
        static readonly maxRect: Rectangle;
        /**
         * 返回此矩形左边缘的X坐标
         */
        readonly left: number;
        /**
         * 返回此矩形右边缘的X坐标
         */
        readonly right: number;
        /**
         * 返回此矩形顶边的y坐标
         */
        readonly top: number;
        /**
         * 返回此矩形底边的y坐标
         */
        readonly bottom: number;
        /**
         * 获取矩形的最大点，即右下角
         */
        readonly max: Vector2;
        /**
         * 这个矩形的宽和高是否为0，位置是否为（0，0）
         */
        isEmpty(): boolean;
        /** 这个矩形的左上角坐标 */
        location: Vector2;
        /**
         * 这个矩形的宽-高坐标
         */
        size: Vector2;
        /**
         * 位于这个矩形中心的一个点
         * 如果 "宽度 "或 "高度 "是奇数，则中心点将向下舍入
         */
        readonly center: Vector2;
        _tempMat: Matrix2D;
        _transformMat: Matrix2D;
        /**
         * 创建一个新的Rectanglestruct实例，指定位置、宽度和高度。
         * @param x 创建的矩形的左上角的X坐标
         * @param y 创建的矩形的左上角的y坐标
         * @param width 创建的矩形的宽度
         * @param height 创建的矩形的高度
         */
        constructor(x?: number, y?: number, width?: number, height?: number);
        /**
         * 创建一个给定最小/最大点（左上角，右下角）的矩形
         * @param minX
         * @param minY
         * @param maxX
         * @param maxY
         */
        static fromMinMax(minX: number, minY: number, maxX: number, maxY: number): Rectangle;
        /**
         * 给定多边形的点，计算边界
         * @param points
         * @returns 来自多边形的点
         */
        static rectEncompassingPoints(points: Vector2[]): Rectangle;
        /**
         * 获取指定边缘的位置
         * @param edge
         */
        getSide(edge: Edge): number;
        /**
         * 获取所提供的坐标是否在这个矩形的范围内
         * @param x 检查封堵点的X坐标
         * @param y 检查封堵点的Y坐标
         */
        contains(x: number, y: number): boolean;
        /**
         * 按指定的水平和垂直方向调整此矩形的边缘
         * @param horizontalAmount 调整左、右边缘的值
         * @param verticalAmount 调整上、下边缘的值
         */
        inflate(horizontalAmount: number, verticalAmount: number): void;
        /**
         * 获取其他矩形是否与这个矩形相交
         * @param value 另一个用于测试的矩形
         */
        intersects(value: Rectangle): boolean;
        rayIntersects(ray: Ray2D, distance: Ref<number>): boolean;
        /**
         * 获取所提供的矩形是否在此矩形的边界内
         * @param value
         */
        containsRect(value: Rectangle): boolean;
        getHalfSize(): Vector2;
        getClosestPointOnBoundsToOrigin(): Vector2;
        /**
         * 返回离给定点最近的点
         * @param point 矩形上离点最近的点
         */
        getClosestPointOnRectangleToPoint(point: Vector2): Vector2;
        /**
         * 获取矩形边界上与给定点最近的点
         * @param point
         * @param edgeNormal
         * @returns 矩形边框上离点最近的点
         */
        getClosestPointOnRectangleBorderToPoint(point: Vector2, edgeNormal: Vector2): Vector2;
        /**
         * 创建一个新的RectangleF，该RectangleF包含两个其他矩形的重叠区域
         * @param value1
         * @param value2
         * @returns 将两个矩形的重叠区域作为输出参数
         */
        static intersect(value1: Rectangle, value2: Rectangle): Rectangle;
        /**
         * 改变这个矩形的位置
         * @param offsetX 要添加到这个矩形的X坐标
         * @param offsetY 要添加到这个矩形的y坐标
         */
        offset(offsetX: number, offsetY: number): void;
        /**
         * 创建一个完全包含两个其他矩形的新矩形
         * @param value1
         * @param value2
         */
        static union(value1: Rectangle, value2: Rectangle): Rectangle;
        /**
         * 在矩形重叠的地方创建一个新的矩形
         * @param value1
         * @param value2
         */
        static overlap(value1: Rectangle, value2: Rectangle): Rectangle;
        calculateBounds(parentPosition: Vector2, position: Vector2, origin: Vector2, scale: Vector2, rotation: number, width: number, height: number): void;
        /**
         * 返回一个横跨当前矩形和提供的三角形位置的矩形
         * @param deltaX
         * @param deltaY
         */
        getSweptBroadphaseBounds(deltaX: number, deltaY: number): Rectangle;
        /**
         * 如果发生碰撞，返回true
         * moveX和moveY将返回b1为避免碰撞而必须移动的移动量
         * @param other
         * @param moveX
         * @param moveY
         */
        collisionCheck(other: Rectangle, moveX: Ref<number>, moveY: Ref<number>): boolean;
        /**
         * 计算两个矩形之间有符号的交点深度
         * @param rectA
         * @param rectB
         * @returns 两个相交的矩形之间的重叠量。
         * 这些深度值可以是负值，取决于矩形/相交的哪些边。
         * 这允许调用者确定正确的推送对象的方向，以解决碰撞问题。
         * 如果矩形不相交，则返回Vector2.Zero
         */
        static getIntersectionDepth(rectA: Rectangle, rectB: Rectangle): Vector2;
        /**
         * 比较当前实例是否等于指定的矩形
         * @param other
         */
        equals(other: Rectangle): boolean;
        /**
         * 获取这个矩形的哈希码
         */
        getHashCode(): number;
        clone(): Rectangle;
    }
}
declare module es {
    class SortUtils {
        protected static defaultCompareFn(a: any, b: any): number;
        /**
         * Sort array elements，
         * @param list any type array
         * @param compareFn Numerical comparison is used by default, { -1, 0 , 1 }
         */
        static sort<T>(list: T[], compareFn?: (a: T, b: T) => number): void;
        protected static _sort<T>(q: T[], l: number, r: number, fn: any): void;
    }
}
declare module es {
    /** 2d 向量 */
    class Vector2 implements IEquatable<Vector2> {
        x: number;
        y: number;
        /**
         * 从两个值构造一个带有X和Y的二维向量。
         * @param x 二维空间中的x坐标
         * @param y 二维空间的y坐标
         */
        constructor(x?: number, y?: number);
        static FromIVector2(ivec2data: {
            x: number;
            y: number;
        }): Vector2;
        static readonly ZERO: Vector2;
        static readonly ONE: Vector2;
        static readonly RIGHT: Vector2;
        static readonly UP: Vector2;
        /**
         *
         * @param value1
         * @param value2
         */
        static add(value1: Vector2, value2: Vector2): Vector2;
        /**
         *
         * @param value1
         * @param value2
         */
        static divide(value1: Vector2, value2: Vector2): Vector2;
        /**
         *
         * @param value1
         * @param value2
         */
        static multiply(value1: Vector2, value2: Vector2): Vector2;
        /**
         *
         * @param value1
         * @param value2
         */
        static subtract(value1: Vector2, value2: Vector2): Vector2;
        /**
         * 创建一个新的Vector2
         * 它包含来自另一个向量的标准化值。
         * @param value
         */
        static normalize(value: Vector2): Vector2;
        /**
         * 返回两个向量的点积
         * @param value1
         * @param value2
         */
        static dot(value1: Vector2, value2: Vector2): number;
        /**
         * 向量缩放
         * @param a 原向量
         * @param r 比例
         * @returns 返回缩放后的原向量
         */
        static scale(a: Vector2, r: number): Vector2;
        /**
         * 自身缩放
         * @param r 比例
         * @returns 返回缩放后的this
         */
        scale(r: number): Vector2;
        /**
         * 计算反弹向量
         * @param inDirection 入射向量
         * @param inNormal 法线
         * @param result 返回反弹向量
         * @returns 反弹向量
         */
        static reflect(inDirection: Vector2, inNormal: Vector2, result: Vector2): Vector2;
        /**
         * src赋值给dest
         * @param src
         * @param dest
         */
        static clonTo(src: Vector2, dest: Vector2): void;
        /**
         * 返回两个向量之间距离的平方
         * @param value1
         * @param value2
         */
        static distanceSquared(value1: Vector2, value2: Vector2): number;
        /**
         * 将指定的值限制在一个范围内
         * @param value1
         * @param min
         * @param max
         */
        static clamp(value1: Vector2, min: Vector2, max: Vector2): Vector2;
        /**
         * 创建一个新的Vector2，其中包含指定向量的线性插值
         * @param value1 第一个向量
         * @param value2 第二个向量
         * @param amount 加权值(0.0-1.0之间)
         * @returns 指定向量的线性插值结果
         */
        static lerp(value1: Vector2, value2: Vector2, amount: number): Vector2;
        /**
         * 创建一个新的Vector2，该Vector2包含了通过指定的Matrix进行的二维向量变换。
         * @param position
         * @param matrix
         */
        static transform(position: Vector2, matrix: Matrix2D): Vector2;
        /**
         * 返回两个向量之间的距离
         * @param value1
         * @param value2
         * @returns 两个向量之间的距离
         */
        static distance(value1: Vector2, value2: Vector2): number;
        /**
         * 返回两个向量之间的角度，单位是度数
         * @param from
         * @param to
         */
        static angle(from: Vector2, to: Vector2): number;
        /**
         * 创建一个包含指定向量反转的新Vector2
         * @param value
         * @returns 矢量反演的结果
         */
        static negate(value: Vector2): Vector2;
        /**
         *
         * @param value
         */
        add(value: Vector2): Vector2;
        /**
         *
         * @param value
         */
        divide(value: Vector2): Vector2;
        /**
         *
         * @param value
         */
        multiply(value: Vector2): Vector2;
        /**
         * 从当前Vector2减去一个Vector2
         * @param value 要减去的Vector2
         * @returns 当前Vector2
         */
        subtract(value: Vector2): this;
        /**
         * 将这个Vector2变成一个方向相同的单位向量
         */
        normalize(): void;
        /** 返回它的长度 */
        length(): number;
        /**
         * 返回该Vector2的平方长度
         * @returns 这个Vector2的平方长度
         */
        lengthSquared(): number;
        /**
         * 四舍五入X和Y值
         */
        round(): Vector2;
        /**
         * 返回以自己为中心点的左右角，单位为度
         * @param left
         * @param right
         */
        angleBetween(left: Vector2, right: Vector2): number;
        /**
         * 比较当前实例是否等于指定的对象
         * @param other 要比较的对象
         * @returns 如果实例相同true 否则false
         */
        equals(other: {
            x: number;
            y: number;
        }): boolean;
        equalf(other: {
            x: number;
            y: number;
        }, precision?: number): boolean;
        clone(): Vector2;
    }
}
declare module es {
    class Vector2Ext {
        /**
         * 检查三角形是CCW还是CW
         * @param a
         * @param center
         * @param c
         */
        static isTriangleCCW(a: Vector2, center: Vector2, c: Vector2): boolean;
        static halfVector(): Vector2;
        /**
         * 计算二维伪叉乘点(Perp(u)， v)
         * @param u
         * @param v
         */
        static cross(u: Vector2, v: Vector2): number;
        /**
         * 返回垂直于传入向量的向量
         * @param first
         * @param second
         */
        static perpendicular(first: Vector2, second: Vector2): Vector2;
        /**
         * 将x/y值翻转，并将y反转，得到垂直于x/y的值
         * @param original
         */
        static perpendicularFlip(original: Vector2): Vector2;
        /**
         * 返回两个向量之间的角度，单位为度
         * @param from
         * @param to
         */
        static angle(from: Vector2, to: Vector2): number;
        /**
         * 给定两条直线(ab和cd)，求交点
         * @param a
         * @param b
         * @param c
         * @param d
         * @param intersection
         */
        static getRayIntersection(a: Vector2, b: Vector2, c: Vector2, d: Vector2, intersection?: Vector2): boolean;
        /**
         * Vector2的临时解决方案
         * 标准化把向量弄乱了
         * @param vec
         */
        static normalize(vec: Vector2): void;
        /**
         * 通过指定的矩阵对Vector2的数组中的向量应用变换，并将结果放置在另一个数组中。
         * @param sourceArray
         * @param sourceIndex
         * @param matrix
         * @param destinationArray
         * @param destinationIndex
         * @param length
         */
        static transformA(sourceArray: Vector2[], sourceIndex: number, matrix: Matrix2D, destinationArray: Vector2[], destinationIndex: number, length: number): void;
        /**
         * 创建一个新的Vector2，该Vector2包含了通过指定的Matrix进行的二维向量变换
         * @param position
         * @param matrix
         * @param result
         */
        static transformR(position: Vector2, matrix: Matrix2D, result?: Vector2): void;
        /**
         * 通过指定的矩阵对Vector2的数组中的所有向量应用变换，并将结果放到另一个数组中。
         * @param sourceArray
         * @param matrix
         * @param destinationArray
         */
        static transform(sourceArray: Vector2[], matrix: Matrix2D, destinationArray: Vector2[]): void;
        static round(vec: Vector2): Vector2;
    }
}
declare module es {
    abstract class IMono {
        protected enableGUI(): void;
        protected disableGUI(): void;
        abstract OnGUI(): void;
        protected Print(any: string): void;
    }
}
declare module es {
    class ObjPool {
        /**
         * 预加载指定数量的对象
         * @param key 作为唯一的key
         * @param cls 一个可以new的对象
         * @param count 预加载的数量
         * @param foreach 每个item实例化后都会回调的方法
         */
        static Reserve(key: string, cls: new () => any, count: number, foreach?: (item: any, index: number) => void): void;
        static Spawn<T>(key: string): T;
        static Despawn(obj: any): void;
    }
}
declare module es {
    enum PointSectors {
        center = 0,
        top = 1,
        bottom = 2,
        topLeft = 9,
        topRight = 5,
        left = 8,
        right = 4,
        bottomLeft = 10,
        bottomRight = 6
    }
    class Collisions {
        static lineToLine(a1: Vector2, a2: Vector2, b1: Vector2, b2: Vector2): boolean;
        static lineToLineIntersection(a1: Vector2, a2: Vector2, b1: Vector2, b2: Vector2, intersection?: Vector2): boolean;
        static closestPointOnLine(lineA: Vector2, lineB: Vector2, closestTo: Vector2): Vector2;
        static circleToCircle(circleCenter1: Vector2, circleRadius1: number, circleCenter2: Vector2, circleRadius2: number): boolean;
        static circleToLine(circleCenter: Vector2, radius: number, lineFrom: Vector2, lineTo: Vector2): boolean;
        static circleToPoint(circleCenter: Vector2, radius: number, point: Vector2): boolean;
        static rectToCircle(rect: Rectangle, cPosition: Vector2, cRadius: number): boolean;
        static rectToLine(rect: Rectangle, lineFrom: Vector2, lineTo: Vector2): boolean;
        static rectToPoint(rX: number, rY: number, rW: number, rH: number, point: Vector2): boolean;
        /**
         * 位标志和帮助使用Cohen–Sutherland算法
         *
         * 位标志:
         * 1001 1000 1010
         * 0001 0000 0010
         * 0101 0100 0110
         * @param rX
         * @param rY
         * @param rW
         * @param rH
         * @param point
         */
        static getSector(rX: number, rY: number, rW: number, rH: number, point: Vector2): PointSectors;
    }
}
declare module es {
    /**
     * 不是真正的射线(射线只有开始和方向)，作为一条线和射线。
     */
    class Ray2D {
        start: Vector2;
        end: Vector2;
        direction: Vector2;
        constructor(position: Vector2, end: Vector2);
    }
}
declare module es {
    class SpineFactory {
        /** 版本号 */
        protected static version: Laya.SpineVersion;
        /** 缓存池 */
        protected static cachePool: {
            [url: string]: Laya.SpineTemplet;
        };
        /** 加载完成事件 */
        protected static loadedCallback: Map<Laya.SpineTemplet, Laya.Handler[]>;
        /**
         * 初始化
         * @param ver Spine版本号
         */
        static init(ver: Laya.SpineVersion): void;
        /**
         * 提前预加载
         * @param url .skel文件url
         * @param cb 加载完成回调
         */
        static load(url: string, cb: Laya.Handler): void;
        /**
         * 提前预加载
         * @param urls  Array<.skel>
         * @param cb 加载完成回调
         */
        static loads(urls: string[], cb: Laya.Handler): void;
        /** 添加 加载结束回调 */
        private static bindLoadedCallback;
        /** 加载完成 */
        protected static loadComplete(obj: any): void;
        /** 加载失败 */
        protected static loadFail(e: any): void;
        /**
         * 生成新的龙骨
         * @param in_url 龙骨url
         * @param out_res 加载完成后的回调
         */
        static spawn(in_url: string, out_res: (spine: Laya.SpineSkeleton) => void): void;
        /**
         * 添加一个动画播放停止的事件监听
         * @param spine
         * @param handler
         * @param once
         */
        static AddCompleteHandle(spine: Laya.SpineSkeleton, caller: any, func: Function, once?: boolean): void;
        /**
         * 移除Spine动画 停止事件监听
         *
         * @param spine 指定的Laya.SpineSkeleton实例
         * @param caller 如果 caller 和 func 都存在 则指定删除，如果仅有caller，则移除spine上所有关于caller的监听
         * @param func 仅配合caller使用 指定删除某对象某一方法
         */
        static RemoveCompleteHandle(spine: Laya.SpineSkeleton, caller?: any, func?: Function): void;
    }
}
declare module es {
    class SpinePool {
        protected static _pool: Laya.SpineSkeleton[];
        static PushBack(spineInst: Laya.SpineSkeleton, sign?: string): void;
        protected static Sign(poolSign: string, spineInst: Laya.SpineSkeleton): void;
        protected static Clean(spineInst: Laya.SpineSkeleton): void;
        static Malloc(sign: string): any;
        static Recycle(spineInst: Laya.SpineSkeleton): void;
        static TrackAllSpineRecycle2LayaPool(): void;
    }
}
declare module es {
    /**
     *  第三方消息派发工具类
     */
    class Third {
        private static _handls;
        /**
         * 监听事件
         * @param event 事件名称
         * @param caller 作用域
         * @param handl 回调方法
         */
        static on(event: string, caller: any, handl: Function): void;
        /**
         * 注册节点的特定事件类型回调，回调会在第一时间被触发后删除自身
         * @param event 事件名称
         * @param caller 作用域
         * @param handl 回调方法
         */
        static once(event: string, caller: any, handl: Function): void;
        /**
         * 移除监听
         * @param event 事件名称
         * @param caller 作用域 如果忽略 则移除所有该事件的监听 off("show");//所有show事件相关监听都会被移除
         * @param handl 回调方法 如果忽略 则移除该事件列表里 所有caller相关的方法  off("show",this);// this作用域下所有对show的监听都会被移除
         */
        static off(event: string, caller?: any, handl?: Function): void;
        /**
         * 移除监听者的所有事件监听
         * @param caller 监听者 如果为null 则进行clear   offall(); 事件池归零
         */
        static offall(caller?: any): void;
        /**
         * 派发事件
         * @param event 事件
         * @param params 可变参列表  do("show",true);   do("show",true,data);  do("show",data,true,"hello world",1234567890);
         */
        static do(event: string, ...params: any[]): void;
    }
}
declare module es {
    /**
     * Class ThirdSupport
     *
     * .You can inherit it to gain the ability to dispatch events
     *
     * @anchor ChenJC | Geek7
     * @date 20220615
     */
    class ThirdSupport {
        protected persistentHandler: Map<string, es.Functor[]>;
        protected onceHandler: Map<string, es.Functor[]>;
        on(eventType: string, caller: any, fn: Function): void;
        once(eventType: string, caller: any, fn: Function): void;
        event(eventType: string): void;
        eventArgs(eventType: string, ...args: any[]): void;
        /**
         *
         * @param eventType if this value is null, then delete all handelr for caller
         * @param caller if this value is null and eventType is null, then clear all handler
         * @param fn if this value is null, then clear caller all method.
         */
        off(eventType?: string, caller?: any, fn?: Function): void;
    }
}
declare module es {
    /**
     * 所有的 TimelineSprite 对象 统一调度管理
     * 全局只产生一个定时器
     */
    class TimelineFactory {
        /**
         * 减少内存碎片 使用单例管理一个类申请的所有内存
         */
        private static instance;
        private elements;
        /**
         * 不允许自行创建
         */
        private constructor();
        /**
         * 不允许业务逻辑自行创建
         */
        private static genSingleton;
        /**
         * 更新
         */
        private update;
        appendTimeline(timeline: TimelineSprite): void;
        removeTimeline(timelineOrIndex: TimelineSprite | number): void;
        /**
         * 主动释放
         * 一般情况下 整个游戏生命周期内 都不需要调用 你可以忽视它
         */
        release(): void;
    }
}
declare module es {
    /**
     * 使用案例
     * https://blog.csdn.net/qq_39162566/article/details/125105592
     */
    class TimelineSprite extends Laya.Sprite {
        protected interval: number;
        protected tm: number;
        protected play_begin_pos: number;
        protected play_end_pos: number;
        protected loop: boolean;
        protected index: number;
        protected frames: string[];
        protected frameEvent: {
            [frameID: number]: Laya.Handler[];
        };
        protected last_frame: Laya.Texture;
        /**
         * 最后一帧派发
         */
        static readonly EVENT_ENDFRAME: string;
        /**
         * 非循环播放结束时触发
         */
        static readonly EVENT_ENDED: string;
        /**
         * 构造方法
         * @param frames 动画帧数组
         * @param frameRate 帧率 例如: frameRate = 30 那每帧的间隔就是33.33ms ( 1000 / 30 )
         */
        constructor(frames: string[], frameRate?: number);
        /**
         * 设置新的动画组
         * @param frames 动画帧数组
         * @param frameRate 帧率 可以不填 采用上一个动画的方案 默认30帧每秒
         */
        setNewAnimationFrames(frames: string[], frameRate?: number): void;
        /**
         * 播放
         * @param loop 是否循环播放  默认false
         * @param start 开始播放的位置 百分百 0~1  循环中也是以这个为主
         * @param end 结束播放的位置 百分比 0～1
         */
        play(loop?: boolean, start?: number, end?: number): void;
        /**
         * 添加一个事件
         * @param pos 位置 百分比 0～1
         * @param caller
         * @param fun
         * @param once 是否执行一次后自动移除
         */
        addEvent(pos: number, caller: any, fun: Function, once?: boolean): void;
        /**
         * 停止播放
         * 会停留在当前帧
         * 你可以使用 visible = false; 来隐藏 动画展示
         */
        stop(): void;
        /**
         *
         * 更新帧
         */
        protected updateFrame(): void;
        protected onframeEvent(): void;
        private draw;
        protected add2aniPool(): void;
        protected removeFromPool(): void;
        clone(): TimelineSprite;
    }
}
declare module es {
    /** 新建一个tween动作 */
    function tween(target: any): tween_action;
    /**
     * 获取未完成的tween动作数量
     * @param target 执行tween的对象
     * @returns
     */
    function getUndoneActionCount(target: any): number;
    /**
     * 清除action
     * @param target 执行tween的对象
     * @param quickComplete 是否立即完成剩余动作,会顺序执行每个阶段最后一次update,包括回调方法
     */
    function killTween(target: any, quickComplete?: boolean): void;
    /** tween工厂 */
    class tween_core {
        private _actions;
        private update;
        private add;
        private sub;
        private _timerID;
        /** 停止tween所有tick  PS: 此操作会停下所有的动作 */
        stop(): void;
        /** 开启/恢复tween所有tick */
        resume(): void;
        /** 清理所有tween */
        clear(): void;
        private static _inst;
        static readonly inst: tween_core;
        private constructor();
    }
    /** 维护tween链 */
    class tween_action {
        protected _target: any;
        protected _index: number;
        protected _commands: command[];
        protected _times: number;
        protected init(target: any): void;
        /**
         * 缓动
         * @param dur 持续时间
         * @param props 目标属性
         * @param easeFn ease处理方法 例如: Laya.Ease.backInOut
         */
        to(dur: number, props: any, easeFn?: any): this;
        /**
         * 贝塞尔运动
         * es.tween(this.sprite).bezier(1.0,new Laya.Point(0,0),
         *      new Laya.Point(100,100),
         *      new Laya.Point(300,0)).call( this.callbackFunction ).start();
         *
         */
        bezier(dur: number, startPoint: Laya.Point, peakPoint: Laya.Point, endPoint: Laya.Point): this;
        /**
         * 加入一个回调
         * @param fn 回调
         */
        call(fn: Function): this;
        /**
         * 在动作间添加一个间隔
         * @param time 间隔秒
         */
        delay(time: number): this;
        /**
         * 循环 当 times < 0 无限循环  为0时 不执行动作  为1时执行一次
         * @param times 次数 默认1
         */
        loop(times?: number): this;
        /**
         * 开启缓动
         */
        start(): void;
        /** 清除动作 是否立即完成所有动作 */
        clear(immediately: any): void;
        /** 获取未完成的动作数量 */
        getUndoneActionCount(): number;
        protected fullPreStateDest(curren: command): void;
        protected resume(): void;
        protected update(dt: any): void;
        protected isDone(): boolean;
    }
    /** 基础command, 主要对属性进行线性插值 */
    class command {
        _tm: number;
        _dur: number;
        _src: {};
        _dest: {};
        _target: any;
        _easeHandler: (t: number, b: number, c: number, d: number) => number;
        constructor(target: any);
        isDone(): boolean;
        update(dt: any): void;
        /** 立即完成当前命令 */
        immediatelyComplete(): void;
    }
}
