"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var es;
(function (es) {
    var Ref = /** @class */ (function () {
        function Ref(value) {
            this.value = value;
        }
        return Ref;
    }());
    es.Ref = Ref;
})(es || (es = {}));
var es;
(function (es) {
    var Common = /** @class */ (function () {
        function Common() {
        }
        /**
         * 浅拷贝
         * @param src 拷贝对象
         * @param dest 覆盖对象
         */
        Common.Clone = function (src, dest) {
            for (var k in src) {
                dest[k] = src[k];
            }
        };
        return Common;
    }());
    es.Common = Common;
})(es || (es = {}));
var es;
(function (es) {
    var Debug = /** @class */ (function () {
        function Debug() {
        }
        /**
         * 打印
         * @param content 文本内容
         * @param fontColor 字体颜色
         * @param bgColor 背景颜色
         */
        Debug.log = function (content, fontColor, bgColor) {
            if (fontColor === void 0) { fontColor = "#eeee33"; }
            if (bgColor === void 0) { bgColor = "#000000"; }
            Debug.debug && Debug.eventLog(content) && console.log("%c" + content, "color:" + fontColor + ";background:" + bgColor + ";padding:3px 6px;");
        };
        /**
         * 打印报错
         * @param content 文本内容
         */
        Debug.error = function (content) {
            this.log("############" + content + "############", "#ff0000", "#000000");
        };
        Debug.eventLog = function (content) {
            es.Third.do(Debug.EVENT_DEBUG_LOG, content);
            return true;
        };
        /**
         * 获取当前平台信息
         */
        Debug.getPlatformInfo = function () {
            return {
                /** 当前浏览器 user-Agent 字符串*/
                userAgent: navigator.userAgent.toLowerCase(),
                /** 当前运行在安卓平台上 */
                onAndroid: Boolean(navigator.userAgent.match(/android/ig)),
                /** 当前运行在苹果平台上 */
                onIOS: Boolean(navigator.userAgent.match(/iphone|ipod/ig)),
                /** 当前运行在Ipad平台上 */
                onIpad: Boolean(navigator.userAgent.match(/ipad/ig)),
                /** 当前运行在微信平台上 */
                onWechat: Boolean(navigator.userAgent.match(/MicroMessenger/ig))
            };
        };
        Object.defineProperty(Debug, "gfx", {
            get: function () {
                if (!Debug._gfx) {
                    // //找到活动中的场景
                    // for (let view of Laya.Scene.unDestroyedScenes) {
                    //     if (view._parent) {
                    //         let sprite = new Laya.Sprite();
                    //         sprite.name = "DEBUG_GFX_SPRITE";
                    //         view.addChild(sprite);
                    //         Debug._gfx = sprite.graphics;
                    //         break;
                    //     }
                    // }
                    var sprite = new Laya.Sprite();
                    sprite.name = "DEBUG_GFX_SPRITE";
                    Laya.Scene.root.parent.addChild(sprite);
                    Debug._gfx = sprite.graphics;
                    //调一个定时器结束自动清理
                    Laya.lateTimer.loop(1, this, function () {
                        Debug._gfx.clear(true);
                    });
                }
                return Debug._gfx;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 绘制debug线框
         * @param star 开始 global pos
         * @param end 结束
         * @param color 颜色
         */
        Debug.DrawLine = function (star, end, color) {
            if (color === void 0) { color = "#ff0000"; }
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.drawLine(star.x, star.y, end.x, end.y, color, 2);
            }
        };
        /**
         * 绘制边框
         * @param posOrRect  global pos
         * @param sizeOrColor
         * @param color
         */
        Debug.DrawRectangles = function (posOrRect, sizeOrColor, color) {
            if (Debug.debug && Debug.gfx) {
                if (!posOrRect['width']) {
                    posOrRect['width'] = sizeOrColor['x'];
                    posOrRect['height'] = sizeOrColor['y'];
                    color = color || "#ff0000";
                }
                if (typeof (sizeOrColor) == "string") {
                    color = sizeOrColor || "#ff0000";
                }
                Debug.gfx.drawRect(posOrRect.x, posOrRect.y, posOrRect['width'], posOrRect['height'], undefined, color, 2);
            }
        };
        /**
         * 绘制圆框
         * @param pos
         * @param r
         * @param color
         */
        Debug.DrawCircle = function (pos, r, color) {
            if (color === void 0) { color = "ff0000"; }
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.drawCircle(pos.x, pos.y, r, undefined, color, 2);
            }
        };
        /**
         * 绘制扇形
         * @param pos 位置
         * @param r 半径
         * @param startAngle 开始角度
         * @param endAngle end角度
         * @param color 颜色
         */
        Debug.DrawPie = function (pos, r, startAngle, endAngle, color) {
            if (color === void 0) { color = "ff0000"; }
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.drawPie(pos.x, pos.y, r, startAngle, endAngle, undefined, color, 2);
            }
        };
        /**
         * 绘制文字
         * @param text 内容
         * @param pos 位置
         * @param color 字体颜色
         * @param fontSize 字体像素大小
         */
        Debug.GUILabel = function (text, pos, color, fontSize) {
            if (color === void 0) { color = "ffffff"; }
            if (fontSize === void 0) { fontSize = 20; }
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.fillText(text, pos.x, pos.y, fontSize + "px Arial", color, "left");
            }
        };
        /** 是否在debug模式 */
        Debug.debug = true;
        /**
         * DEBUG模式下 打印日志的内容派发
         */
        Debug.EVENT_DEBUG_LOG = "EVENT_DEBUG_LOG";
        return Debug;
    }());
    es.Debug = Debug;
})(es || (es = {}));
var es;
(function (es) {
    var Edge;
    (function (Edge) {
        Edge[Edge["top"] = 0] = "top";
        Edge[Edge["bottom"] = 1] = "bottom";
        Edge[Edge["left"] = 2] = "left";
        Edge[Edge["right"] = 3] = "right";
    })(Edge = es.Edge || (es.Edge = {}));
})(es || (es = {}));
var es;
(function (es) {
    var Functor = /** @class */ (function () {
        function Functor(fn, caller) {
            this.caller = caller;
            this.fn = fn;
        }
        /**@static 创建一个仿函数 */
        Functor.New = function (fn, caller) {
            return new Functor(fn, caller);
        };
        /**@static 判断两个仿函数是否相等 */
        Functor.Equal = function (me, he) {
            return me.caller == he.caller && me.fn == he.fn;
        };
        /** 通过调用者 和 方法来判断是否相等 */
        Functor.prototype.equal2 = function (caller, fn) {
            return this.caller == caller && this.fn == fn;
        };
        /** 判断两个仿函数是否相等 */
        Functor.prototype.equal = function (he) {
            return Functor.Equal(this, he);
        };
        /** 获取调用者 */
        Functor.prototype.getCaller = function () {
            return this.caller;
        };
        /** 获取方法 */
        Functor.prototype.getMethod = function () {
            return this.fn;
        };
        /** 调用 */
        Functor.prototype.invoke = function () {
            return this.fn.call(this.caller);
        };
        /** 带参数调用 */
        Functor.prototype.invokeArgs = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = this.fn).call.apply(_a, __spread([this.caller], args));
        };
        /** 重写toString */
        Functor.prototype.toString = function () {
            return this.caller.toString() + "." + this.fn.toString() + "(...args)";
        };
        return Functor;
    }());
    es.Functor = Functor;
})(es || (es = {}));
var es;
(function (es) {
    var Hash = /** @class */ (function () {
        function Hash() {
        }
        /**
         * 从一个字节数组中计算一个哈希值
         * @param data
         */
        Hash.computeHash = function () {
            var data = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                data[_i] = arguments[_i];
            }
            var p = 16777619;
            var hash = 2166136261;
            for (var i = 0; i < data.length; i++)
                hash = (hash ^ data[i]) * p;
            hash += hash << 13;
            hash ^= hash >> 7;
            hash += hash << 3;
            hash ^= hash >> 17;
            hash += hash << 5;
            return hash;
        };
        return Hash;
    }());
    es.Hash = Hash;
})(es || (es = {}));
var es;
(function (es) {
    var CharType;
    (function (CharType) {
        CharType[CharType["Chinese"] = 0] = "Chinese";
        CharType[CharType["Number"] = 1] = "Number";
        CharType[CharType["Lowercase"] = 2] = "Lowercase";
        CharType[CharType["Uppercase"] = 3] = "Uppercase";
        CharType[CharType["Symbol"] = 4] = "Symbol";
    })(CharType = es.CharType || (es.CharType = {}));
    var StringTool = /** @class */ (function () {
        function StringTool() {
        }
        /**
    * 字符检测
    * @param _char 待判断的字符
    * @returns
    */
        StringTool.charTest = function (_char) {
            var regs = [
                /[\u4E00-\u9FA5]+/,
                /[0-9]+/,
                /[a-z]+/,
                /[A-Z]+/,
            ];
            var index = [].findIndex.call(regs, function (reg, index) {
                if (reg.test(_char))
                    return true;
            });
            return (index == -1 ? 4 : index); //.,!符号
        };
        /**
         * 返回全大写
         * @param str
         * @return
         */
        StringTool.toUpCase = function (str) {
            return str.toUpperCase();
        };
        /**
         * 返回全小写
         * @param str
         * @return
         */
        StringTool.toLowCase = function (str) {
            return str.toLowerCase();
        };
        /**
         * 返回首字母大写
         * @param str
         * @return
         */
        StringTool.toUpHead = function (str) {
            var rst;
            if (str.length <= 1)
                return str.toUpperCase();
            rst = str.charAt(0).toUpperCase() + str.substring(1);
            return rst;
        };
        /**
         * 返回首字母小写
         * @param str
         * @return
         */
        StringTool.toLowHead = function (str) {
            var rst;
            if (str.length <= 1)
                return str.toLowerCase();
            rst = str.charAt(0).toLowerCase() + str.substring(1);
            return rst;
        };
        /**
         * 包名转路径名
         * @param packageName
         * @return
         */
        StringTool.packageToFolderPath = function (packageName) {
            var rst;
            rst = packageName.replace(".", "/");
            return rst;
        };
        StringTool.insert = function (str, iStr, index) {
            return str.substring(0, index) + iStr + str.substring(index);
        };
        StringTool.insertAfter = function (str, iStr, tarStr, isLast) {
            if (isLast === void 0) { isLast = false; }
            var i;
            if (isLast) {
                i = str.lastIndexOf(tarStr);
            }
            else {
                i = str.indexOf(tarStr);
            }
            if (i >= 0) {
                return StringTool.insert(str, iStr, i + tarStr.length);
            }
            return str;
        };
        StringTool.insertBefore = function (str, iStr, tarStr, isLast) {
            if (isLast === void 0) { isLast = false; }
            var i;
            if (isLast) {
                i = str.lastIndexOf(tarStr);
            }
            else {
                i = str.indexOf(tarStr);
            }
            if (i >= 0) {
                return StringTool.insert(str, iStr, i);
            }
            return str;
        };
        StringTool.insertParamToFun = function (funStr, params) {
            var oldParam;
            oldParam = StringTool.getParamArr(funStr);
            var inserStr;
            inserStr = params.join(",");
            if (oldParam.length > 0) {
                inserStr = "," + inserStr;
            }
            return StringTool.insertBefore(funStr, inserStr, ")", true);
        };
        /**
         * 去除空格和换行符
         * @param str
         * @return
         */
        StringTool.trim = function (str, vList) {
            if (vList === void 0) { vList = null; }
            if (!vList) {
                vList = [" ", "\r", "\n", "\t", String.fromCharCode(65279)];
            }
            var rst;
            var i;
            var len;
            rst = str;
            len = vList.length;
            for (i = 0; i < len; i++) {
                rst = StringTool.getReplace(rst, vList[i], "");
            }
            return rst;
        };
        StringTool.isEmpty = function (str) {
            if (str.length < 1)
                return true;
            return StringTool.emptyStrDic.hasOwnProperty(str);
        };
        StringTool.trimLeft = function (str) {
            var i;
            i = 0;
            var len;
            len = str.length;
            while (StringTool.isEmpty(str.charAt(i)) && i < len) {
                i++;
            }
            if (i < len) {
                return str.substring(i);
            }
            return "";
        };
        StringTool.trimRight = function (str) {
            var i;
            i = str.length - 1;
            //			trace(str+" "+str.length);
            while (StringTool.isEmpty(str.charAt(i)) && i >= 0) {
                //				i=str.length-1;
                i--;
            }
            //			trace(str.charAt(i));
            var rst;
            rst = str.substring(0, i);
            //			trace(rst+" "+rst.length);
            if (i >= 0) {
                return str.substring(0, i + 1);
            }
            return "";
        };
        StringTool.trimSide = function (str) {
            var rst;
            rst = StringTool.trimLeft(str);
            rst = StringTool.trimRight(rst);
            return rst;
        };
        StringTool.isOkFileName = function (fileName) {
            if (StringTool.trimSide(fileName) == "")
                return false;
            var i, len;
            len = fileName.length;
            for (i = 0; i < len; i++) {
                if (StringTool.specialChars[fileName.charAt(i)])
                    return false;
            }
            return true;
        };
        StringTool.trimButEmpty = function (str) {
            return StringTool.trim(str, ["\r", "\n", "\t"]);
        };
        StringTool.removeEmptyStr = function (strArr) {
            var i;
            i = strArr.length - 1;
            var str;
            for (i = i; i >= 0; i--) {
                str = strArr[i];
                str = StringTool.trimSide(str);
                if (StringTool.isEmpty(str)) {
                    strArr.splice(i, 1);
                }
                else {
                    strArr[i] = str;
                }
            }
            return strArr;
        };
        StringTool.ifNoAddToTail = function (str, sign) {
            if (str.indexOf(sign) >= 0) {
                return str;
            }
            return str + sign;
        };
        StringTool.trimEmptyLine = function (str) {
            var i;
            var len;
            var tLines;
            var tLine;
            tLines = str.split("\n");
            for (i = tLines.length - 1; i >= 0; i--) {
                tLine = tLines[i];
                if (StringTool.isEmptyLine(tLine)) {
                    tLines.splice(i, 1);
                }
            }
            return tLines.join("\n");
        };
        StringTool.isEmptyLine = function (str) {
            str = StringTool.trim(str);
            if (str == "")
                return true;
            return false;
        };
        StringTool.removeCommentLine = function (lines) {
            var rst;
            rst = [];
            var i;
            var tLine;
            var adptLine;
            i = 0;
            var len;
            var index;
            len = lines.length;
            while (i < len) {
                adptLine = tLine = lines[i];
                index = tLine.indexOf("/**");
                if (index >= 0) {
                    adptLine = tLine.substring(0, index - 1);
                    StringTool.addIfNotEmpty(rst, adptLine);
                    //				    i++;
                    while (i < len) {
                        tLine = lines[i];
                        index = tLine.indexOf("*/");
                        if (index >= 0) {
                            adptLine = tLine.substring(index + 2);
                            StringTool.addIfNotEmpty(rst, adptLine);
                            break;
                        }
                        i++;
                    }
                }
                else if (tLine.indexOf("//") >= 0) {
                    if (StringTool.trim(tLine).indexOf("//") == 0) {
                    }
                    else {
                        StringTool.addIfNotEmpty(rst, adptLine);
                    }
                }
                else {
                    StringTool.addIfNotEmpty(rst, adptLine);
                }
                i++;
            }
            return rst;
        };
        StringTool.addIfNotEmpty = function (arr, str) {
            if (!str)
                return;
            var tStr;
            tStr = StringTool.trim(str);
            if (tStr != "") {
                arr.push(str);
            }
        };
        StringTool.trimExt = function (str, vars) {
            var rst;
            rst = StringTool.trim(str);
            var i;
            var len;
            len = vars.length;
            for (i = 0; i < len; i++) {
                rst = StringTool.getReplace(rst, vars[i], "");
            }
            return rst;
        };
        StringTool.getBetween = function (str, left, right, ifMax) {
            if (ifMax === void 0) { ifMax = false; }
            if (!str)
                return "";
            if (!left)
                return "";
            if (!right)
                return "";
            var lId;
            var rId;
            lId = str.indexOf(left);
            if (lId < 0)
                return "";
            if (ifMax) {
                rId = str.lastIndexOf(right);
                if (rId < lId)
                    return "";
            }
            else {
                rId = str.indexOf(right, lId + 1);
            }
            if (rId < 0)
                return "";
            return str.substring(lId + left.length, rId);
        };
        StringTool.getSplitLine = function (line, split) {
            if (split === void 0) { split = " "; }
            return line.split(split);
        };
        StringTool.getLeft = function (str, sign) {
            var i;
            i = str.indexOf(sign);
            return str.substring(0, i);
        };
        StringTool.getRight = function (str, sign) {
            var i;
            i = str.indexOf(sign);
            return str.substring(i + 1);
        };
        StringTool.delelteItem = function (arr) {
            while (arr.length > 0) {
                if (arr[0] == "") {
                    arr.shift();
                }
                else {
                    break;
                }
            }
        };
        StringTool.getWords = function (line) {
            var rst = StringTool.getSplitLine(line);
            StringTool.delelteItem(rst);
            return rst;
        };
        StringTool.getLinesI = function (startLine, endLine, lines) {
            var i;
            var rst = [];
            for (i = startLine; i <= endLine; i++) {
                rst.push(lines[i]);
            }
            return rst;
        };
        StringTool.structfy = function (str, inWidth, removeEmpty) {
            if (inWidth === void 0) { inWidth = 4; }
            if (removeEmpty === void 0) { removeEmpty = true; }
            if (removeEmpty) {
                str = StringTool.trimEmptyLine(str);
            }
            var lines;
            var tIn;
            tIn = 0;
            var tInStr;
            tInStr = StringTool.getEmptyStr(0);
            lines = str.split("\n");
            var i;
            var len;
            var tLineStr;
            len = lines.length;
            for (i = 0; i < len; i++) {
                tLineStr = lines[i];
                //				tLineStr=StringTool.trimSide(tLineStr);
                tLineStr = StringTool.trimLeft(tLineStr);
                tLineStr = StringTool.trimRight(tLineStr);
                tIn += StringTool.getPariCount(tLineStr);
                //				if(tLineStr=="}")
                //				{
                //					tLineStr=getEmptyStr(tIn*2)+tLineStr;
                //				}else
                //				{
                //					tLineStr=tInStr+tLineStr;
                //				}
                if (tLineStr.indexOf("}") >= 0) {
                    tInStr = StringTool.getEmptyStr(tIn * inWidth);
                }
                tLineStr = tInStr + tLineStr;
                lines[i] = tLineStr;
                tInStr = StringTool.getEmptyStr(tIn * inWidth);
            }
            return lines.join("\n");
        };
        StringTool.getEmptyStr = function (width) {
            if (!StringTool.emptyDic.hasOwnProperty(width)) {
                var i;
                var len;
                len = width;
                var rst;
                rst = "";
                for (i = 0; i < len; i++) {
                    rst += " ";
                }
                StringTool.emptyDic[width] = rst;
            }
            return StringTool.emptyDic[width];
        };
        StringTool.getPariCount = function (str, inChar, outChar) {
            if (inChar === void 0) { inChar = "{"; }
            if (outChar === void 0) { outChar = "}"; }
            var varDic;
            varDic = {};
            varDic[inChar] = 1;
            varDic[outChar] = -1;
            var i;
            var len;
            var tChar;
            len = str.length;
            var rst;
            rst = 0;
            for (i = 0; i < len; i++) {
                tChar = str.charAt(i);
                if (varDic.hasOwnProperty(tChar)) {
                    rst += varDic[tChar];
                }
            }
            return rst;
        };
        StringTool.readInt = function (str, startI) {
            if (startI === void 0) { startI = 0; }
            var rst;
            rst = 0;
            var tNum;
            var tC;
            var i;
            var isBegin;
            isBegin = false;
            var len;
            len = str.length;
            for (i = startI; i < len; i++) {
                tC = str.charAt(i);
                if (Number(tC) > 0 || tC == "0") {
                    rst = 10 * rst + Number(tC);
                    if (rst > 0)
                        isBegin = true;
                }
                else {
                    if (isBegin)
                        return rst;
                }
            }
            return rst;
        };
        /**
         * 替换文本
         * @param str
         * @param oStr
         * @param nStr
         * @return
         */
        StringTool.getReplace = function (str, oStr, nStr) {
            if (!str)
                return "";
            //			if(!oStr) return str;
            //			if(!nStr) return str;
            var rst;
            rst = str.replace(new RegExp(oStr, "g"), nStr);
            return rst;
        };
        StringTool.getWordCount = function (str, findWord) {
            var rg = new RegExp(findWord, "g");
            return str.match(rg).length;
        };
        StringTool.getResolvePath = function (path, basePath) {
            if (StringTool.isAbsPath(path)) {
                return path;
            }
            var tSign;
            tSign = "\\";
            if (basePath.indexOf("/") >= 0) {
                tSign = "/";
            }
            if (basePath.charAt(basePath.length - 1) == tSign) {
                basePath = basePath.substring(0, basePath.length - 1);
            }
            var parentSign;
            parentSign = ".." + tSign;
            var tISign;
            tISign = "." + tSign;
            var pCount;
            pCount = StringTool.getWordCount(path, parentSign);
            path = StringTool.getReplace(path, parentSign, "");
            path = StringTool.getReplace(path, tISign, "");
            var i;
            var len;
            len = pCount;
            var iPos;
            for (i = 0; i < len; i++) {
                basePath = StringTool.removeLastSign(path, tSign);
            }
            return basePath + tSign + path;
        };
        StringTool.isAbsPath = function (path) {
            if (path.indexOf(":") >= 0)
                return true;
            return false;
        };
        StringTool.removeLastSign = function (str, sign) {
            var iPos;
            iPos = str.lastIndexOf(sign);
            str = str.substring(0, iPos);
            return str;
        };
        StringTool.getParamArr = function (str) {
            var paramStr;
            paramStr = StringTool.getBetween(str, "(", ")", true);
            if (StringTool.trim(paramStr).length < 1)
                return [];
            return paramStr.split(",");
        };
        StringTool.copyStr = function (str) {
            return str.substring(0);
        };
        /**
         * 将Array转成字符串
         * @param arr
         * @return
         */
        StringTool.ArrayToString = function (arr) {
            var rst;
            rst = "[{items}]".replace(new RegExp("\\{items\\}", "g"), StringTool.getArrayItems(arr));
            return rst;
        };
        StringTool.getArrayItems = function (arr) {
            var rst;
            if (arr.length < 1)
                return "";
            rst = StringTool.parseItem(arr[0]);
            var i;
            var len;
            len = arr.length;
            for (i = 1; i < len; i++) {
                rst += "," + StringTool.parseItem(arr[i]);
            }
            return rst;
        };
        StringTool.parseItem = function (item) {
            var rst;
            rst = "\"" + item + "\"";
            return "";
        };
        StringTool.initAlphaSign = function () {
            if (StringTool.alphaSigns)
                return;
            StringTool.alphaSigns = {};
            StringTool.addSign("a", "z", StringTool.alphaSigns);
            StringTool.addSign("A", "Z", StringTool.alphaSigns);
            StringTool.addSign("0", "9", StringTool.alphaSigns);
        };
        StringTool.addSign = function (ss, e, tar) {
            var i;
            var len;
            var s;
            s = ss.charCodeAt(0);
            len = e.charCodeAt(0);
            for (i = s; i <= len; i++) {
                tar[String.fromCharCode(i)] = true;
                console.log("add :" + String.fromCharCode(i));
            }
        };
        StringTool.isPureAlphaNum = function (str) {
            StringTool.initAlphaSign();
            if (!str)
                return true;
            var i, len;
            len = str.length;
            for (i = 0; i < len; i++) {
                if (!StringTool.alphaSigns[str.charAt(i)])
                    return false;
            }
            return true;
        };
        StringTool.format = function (text) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var reg = /{(\d+)}/gm;
            return text.replace(reg, function (match, name) {
                return args[~~name];
            });
        };
        /**
         * 数字位数不足 前面补0
         * @param num 数值
         * @param len 例如 3 =>   ...008,009,010,011
         * @returns
         */
        StringTool.formatZero = function (num, len) {
            if (String(num).length > len) {
                return num;
            }
            return (Array(len).join('0') + num).slice(-len);
        };
        StringTool.emptyStrDic = {
            " ": true,
            "\r": true,
            "\n": true,
            "\t": true
        };
        StringTool.specialChars = { "*": true, "&": true, "%": true, "#": true, "?": true };
        StringTool.emptyDic = {};
        StringTool.alphaSigns = null;
        return StringTool;
    }());
    es.StringTool = StringTool;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * 类型检验工具类
     */
    var TypeUtils = /** @class */ (function () {
        function TypeUtils() {
        }
        /**
         * 检测是否是字符串类型，注意new String检测不通过的
         */
        TypeUtils.isString = function (obj) {
            return typeof obj === 'string' && obj.constructor === String;
        };
        /**
         * 检测是不是数组
         */
        TypeUtils.isArray = function (obj) {
            return obj instanceof Array && Object.prototype.toString.call(obj) === '[object Array]';
        };
        /**
         * 检测是不是数字，注意new Number不算进来
         */
        TypeUtils.isNumber = function (obj) {
            return typeof obj === 'number' && !isNaN(obj); // type NaN === 'number' 所以要去掉
        };
        /**
         * 是不是对象，数组也是一种对象
         */
        TypeUtils.isObject = function (obj) {
            return typeof obj === 'object';
        };
        /**
         * 是不是函数
         */
        TypeUtils.isFunction = function (obj) {
            return typeof obj === 'function';
        };
        /**
         * 检测类型是否相等
         */
        TypeUtils.isSame = function (obj0, obj1) {
            var bool = true;
            if (obj0 != obj1) {
                bool = obj0.__proto__ == obj1.__proto__;
            }
            return bool;
        };
        return TypeUtils;
    }());
    es.TypeUtils = TypeUtils;
})(es || (es = {}));
var es;
(function (es) {
    /*
 * @Description: 数据存储类
 *
 * 将 window.localStorage 和 cookie 揉在一起 兼容不同ie版本
 *
 * 增：EasySave.set(key,value)
 * 删：EasySave.del(key)
 * 改：EasySave.set(key,value)
 * 查：EasySave.has(key)
 *
 */
    var EasySave = /** @class */ (function () {
        function EasySave() {
        }
        /**
         * 存入一个对象
         * @param key
         * @param obj
         */
        EasySave.setObj = function (key, obj) {
            if (typeof obj == "string") {
                EasySave.set(key, obj);
            }
            else {
                try {
                    EasySave.set(key, JSON.stringify(obj));
                }
                catch (e) {
                    console.error(e);
                }
            }
        };
        /**
         * 获取一个对象
         * @param key
         * @returns null | T
         */
        EasySave.getObj = function (key) {
            try {
                var s = EasySave.get(key);
                if (s != null && s != "") {
                    return JSON.parse(s);
                }
                return null;
            }
            catch (e) {
                console.error(e);
                return null;
            }
        };
        /**
         * 存储Cookie
         * @param k 字符串
         * @param v 字符串
         * @param exdays 数据有效日期 默认30天, 仅Cookie有效
         */
        EasySave.set = function (k, v, exdays) {
            if (exdays === void 0) { exdays = 30; }
            if (window.localStorage) {
                window.localStorage.setItem(k, v);
            }
            else {
                var d = new Date();
                d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
                var cookie = k + "=" + v + ";expires=" + d.toUTCString();
                document.cookie = cookie;
            }
        };
        /**
         * 获取Cookie
         * @param k 字符串
         * @returns value | null
         */
        EasySave.get = function (k) {
            if (window.localStorage) {
                return window.localStorage.getItem(k);
            }
            else {
                var name = k + "=";
                var ca = document.cookie.split(";");
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i].trim();
                    if (c.indexOf(name) == 0)
                        return c.substring(name.length, c.length);
                }
            }
            return null;
        };
        /**
         * 判断cookie是否存在
         * @param k
         * @returns true | false
         */
        EasySave.has = function (k) {
            if (window.localStorage) {
                return window.localStorage.getItem(k) != null;
            }
            return EasySave.get(k) != null;
        };
        /**
         * 删除cookie
         * @param k
         */
        EasySave.del = function (k) {
            if (window.localStorage) {
                window.localStorage.removeItem(k);
            }
            else {
                EasySave.set(k, "", -1);
            }
        };
        /**
         * 清理所有Cookie
         */
        EasySave.clear = function () {
            if (window.localStorage) {
                window.localStorage.clear();
            }
            else {
                var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
                if (keys) {
                    for (var i = keys.length; i--;) {
                        document.cookie =
                            keys[i] + "=0;path=/;expires=" + new Date(0).toUTCString(); //清除当前域名下的,例如：m.kevis.com
                        document.cookie =
                            keys[i] +
                                "=0;path=/;domain=" +
                                document.domain +
                                ";expires=" +
                                new Date(0).toUTCString(); //清除当前域名下的，例如 .m.kevis.com
                        document.cookie =
                            keys[i] +
                                "=0;path=/;domain=kevis.com;expires=" +
                                new Date(0).toUTCString(); //清除一级域名下的或指定的，例如 .kevis.com
                    }
                }
            }
        };
        return EasySave;
    }());
    es.EasySave = EasySave;
})(es || (es = {}));
var es;
(function (es) {
    var MathHelper = /** @class */ (function () {
        function MathHelper() {
        }
        /**
         * 将弧度转换成角度。
         * @param radians 用弧度表示的角
         */
        MathHelper.toDegrees = function (radians) {
            return radians * 57.295779513082320876798154814105;
        };
        /**
         * 将角度转换为弧度
         * @param degrees
         */
        MathHelper.toRadians = function (degrees) {
            return degrees * 0.017453292519943295769236907684886;
        };
        /**
         * mapps值(在leftMin - leftMax范围内)到rightMin - rightMax范围内的值
         * @param value
         * @param leftMin
         * @param leftMax
         * @param rightMin
         * @param rightMax
         */
        MathHelper.map = function (value, leftMin, leftMax, rightMin, rightMax) {
            return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
        };
        MathHelper.lerp = function (value1, value2, amount) {
            return value1 + (value2 - value1) * amount;
        };
        MathHelper.clamp = function (value, min, max) {
            if (value < min)
                return min;
            if (value > max)
                return max;
            return value;
        };
        /**
         * 给定圆心、半径和角度，得到圆周上的一个点。0度是3点钟。
         * @param circleCenter
         * @param radius
         * @param angleInDegrees
         */
        MathHelper.pointOnCirlce = function (circleCenter, radius, angleInDegrees) {
            var radians = MathHelper.toRadians(angleInDegrees);
            return new es.Vector2(Math.cos(radians) * radians + circleCenter.x, Math.sin(radians) * radians + circleCenter.y);
        };
        /**
         * 如果值为偶数，返回true
         * @param value
         */
        MathHelper.isEven = function (value) {
            return value % 2 == 0;
        };
        /**
         * 数值限定在0-1之间
         * @param value
         */
        MathHelper.clamp01 = function (value) {
            if (value < 0)
                return 0;
            if (value > 1)
                return 1;
            return value;
        };
        MathHelper.angleBetweenVectors = function (from, to) {
            return Math.atan2(to.y - from.y, to.x - from.x);
        };
        MathHelper.angleToVector = function (angleRadians, length) {
            return new es.Vector2(Math.cos(angleRadians) * length, Math.sin(angleRadians) * length);
        };
        /**
         * 增加t并确保它总是大于或等于0并且小于长度
         * @param t
         * @param length
         */
        MathHelper.incrementWithWrap = function (t, length) {
            t++;
            if (t == length)
                return 0;
            return t;
        };
        /**
         * 以roundToNearest为步长，将值舍入到最接近的数字。例如：在125中找到127到最近的5个结果
         * @param value
         * @param roundToNearest
         */
        MathHelper.roundToNearest = function (value, roundToNearest) {
            return Math.round(value / roundToNearest) * roundToNearest;
        };
        /**
         * 检查传递的值是否在某个阈值之下。对于小规模、精确的比较很有用
         * @param value
         * @param ep
         */
        MathHelper.withinEpsilon = function (value, ep) {
            if (ep === void 0) { ep = this.Epsilon; }
            return Math.abs(value) < ep;
        };
        /**
         * 由上移量向上移。start可以小于或大于end。例如:开始是2，结束是10，移位是4，结果是6
         * @param start
         * @param end
         * @param shift
         */
        MathHelper.approach = function (start, end, shift) {
            if (start < end)
                return Math.min(start + shift, end);
            return Math.max(start - shift, end);
        };
        /**
         * 计算两个给定角之间的最短差值（度数）
         * @param current
         * @param target
         */
        MathHelper.deltaAngle = function (current, target) {
            var num = this.repeat(target - current, 360);
            if (num > 180)
                num -= 360;
            return num;
        };
        /**
         * 循环t，使其永远不大于长度，永远不小于0
         * @param t
         * @param length
         */
        MathHelper.repeat = function (t, length) {
            return t - Math.floor(t / length) * length;
        };
        MathHelper.Epsilon = 0.00001;
        MathHelper.Rad2Deg = 57.29578;
        MathHelper.Deg2Rad = 0.0174532924;
        /**
         * 表示pi除以2的值(1.57079637)
         */
        MathHelper.PiOver2 = Math.PI / 2;
        return MathHelper;
    }());
    es.MathHelper = MathHelper;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * @ignore
     */
    var _d2r = Math.PI / 180.0;
    /**
     * @ignore
     */
    var _r2d = 180.0 / Math.PI;
    /**
     * @property {number} EPSILON
     */
    es.EPSILON = 0.000001;
    // Number of bits in an integer
    es.INT_BITS = 32;
    es.INT_MAX = 0x7fffffff;
    es.INT_MIN = -1 << (es.INT_BITS - 1);
    var Mathf = /** @class */ (function () {
        function Mathf() {
        }
        /**
 * Tests whether or not the arguments have approximately the same value, within an absolute
 * or relative tolerance of glMatrix.EPSILON (an absolute tolerance is used for values less
 * than or equal to 1.0, and a relative tolerance is used for larger values)
 *
 * @param {Number} a The first number to test.
 * @param {Number} b The second number to test.
 * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
 */
        Mathf.equals = function (a, b) {
            return Math.abs(a - b) <= es.EPSILON * Math.max(1.0, Math.abs(a), Math.abs(b));
        };
        /**
         * 插值
         * @param from
         * @param to
         * @param ratio
         * @returns
         */
        Mathf.lerp = function (from, to, ratio) {
            return from + (to - from) * ratio;
        };
        /**
         * 混合
         * @param src
         * @param dest
         * @param ration
         */
        Mathf.mix = function (src, dest, ration) {
            return Mathf.lerp(src, dest, ration);
        };
        /**
         * Returns a floating-point random number between min (inclusive) and max (exclusive).
         *
         * @method randomRange
         * @param {number} min
         * @param {number} max
         * @return {number} the random number
         */
        Mathf.randomRange = function (min, max) {
            return Math.random() * (max - min) + min;
        };
        /**
        * Returns a random integer between min (inclusive) and max (exclusive).
        *
        * @method randomRangeInt
        * @param {number} min
        * @param {number} max
        * @return {number} the random integer
        */
        Mathf.randomRangeInt = function (min, max) {
            return Math.floor(Mathf.randomRange(min, max));
        };
        /**
         * Linear congruential generator using Hull-Dobell Theorem.
         *
         * @method pseudoRandom
         * @param {number} seed the random seed
         * @return {number} the pseudo random
         */
        Mathf.pseudoRandom = function (seed) {
            seed = (seed * 9301 + 49297) % 233280;
            return seed / 233280.0;
        };
        /**
         * Returns a floating-point pseudo-random number between min (inclusive) and max (exclusive).
         *
         * @method pseudoRandomRange
         * @param {number} seed
         * @param {number} min
         * @param {number} max
         * @return {number} the random number
         */
        Mathf.pseudoRandomRange = function (seed, min, max) {
            return Mathf.pseudoRandom(seed) * (max - min) + min;
        };
        /**
 * Returns a pseudo-random integer between min (inclusive) and max (exclusive).
 *
 * @method pseudoRandomRangeInt
 * @param {number} seed
 * @param {number} min
 * @param {number} max
 * @return {number} the random integer
 */
        Mathf.pseudoRandomRangeInt = function (seed, min, max) {
            return Math.floor(Mathf.pseudoRandomRange(seed, min, max));
        };
        /**
 * Returns the next power of two for the value
 *
 * @method nextPow2
 * @param {number} val
 * @return {number} the the next power of two
 */
        Mathf.nextPow2 = function (val) {
            --val;
            val = (val >> 1) | val;
            val = (val >> 2) | val;
            val = (val >> 4) | val;
            val = (val >> 8) | val;
            val = (val >> 16) | val;
            ++val;
            return val;
        };
        /**
         * Returns ratio of a value within a given range
         *
         * @method repeat
         * @param {number} from start value
         * @param {number} to end value
         * @param {number} value given value
         * @return {number} the ratio between [from,to]
         */
        Mathf.inverseLerp = function (from, to, value) {
            return (value - from) / (to - from);
        };
        /**
         * Returns -1, 0, +1 depending on sign of x.
         *
         * @method sign
         * @param {number} v
         */
        Mathf.sign = function (v) {
            return (v > 0) - (v < 0);
        };
        /**
 * Clamps a value between a minimum float and maximum float value.
 *
 * @method clamp
 * @param {number} val
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
        Mathf.clamp = function (val, min, max) {
            return val < min ? min : val > max ? max : val;
        };
        /**
         * Tests whether or not the arguments have approximately the same value by given maxDiff
         *
         * @param {Number} a The first number to test.
         * @param {Number} b The second number to test.
         * @param {Number} maxDiff Maximum difference.
         * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
         */
        Mathf.approx = function (a, b, maxDiff) {
            maxDiff = maxDiff || es.EPSILON;
            return Math.abs(a - b) <= maxDiff;
        };
        /**
        * Convert Degree To Radian
        *
        * @param {Number} a Angle in Degrees
        */
        Mathf.toRadian = function (a) {
            return a * _d2r;
        };
        /**
* Convert Radian To Degree
*
* @param {Number} a Angle in Radian
*/
        Mathf.toDegree = function (a) {
            return a * _r2d;
        };
        /**
         * Determine whether an integer n is a power of 2
         * @param n
         * @returns
         */
        Mathf.isPow = function (n) {
            return (n & (n - 1)) == 0;
        };
        /**
         * Gets the unique, non-repeating value of an integer array
         * @param arr
         * @returns
         */
        Mathf.findChosenOne = function (arr, i, res) {
            if (i === void 0) { i = 1; }
            if (res === void 0) { res = arr[0]; }
            return arr.length <= i ? res : Mathf.findChosenOne(arr, i + 1, res ^ arr[i]);
        };
        return Mathf;
    }());
    es.Mathf = Mathf;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * 代表右手4x4浮点矩阵，可以存储平移、比例和旋转信息
     */
    var Matrix = /** @class */ (function () {
        function Matrix() {
        }
        /**
         * 为自定义的正交视图创建一个新的投影矩阵
         * @param left
         * @param right
         * @param top
         * @param zFarPlane
         * @param result
         */
        Matrix.createOrthographicOffCenter = function (left, right, bottom, top, zNearPlane, zFarPlane, result) {
            if (result === void 0) { result = new Matrix(); }
            result.m11 = 2 / (right - left);
            result.m12 = 0;
            result.m13 = 0;
            result.m14 = 0;
            result.m21 = 0;
            result.m22 = 2 / (top - bottom);
            result.m23 = 0;
            result.m24 = 0;
            result.m31 = 0;
            result.m32 = 0;
            result.m33 = 1 / (zNearPlane - zFarPlane);
            result.m34 = 0;
            result.m41 = (left + right) / (left - right);
            result.m42 = (top + bottom) / (bottom - top);
            result.m43 = zNearPlane / (zNearPlane - zFarPlane);
            result.m44 = 1;
        };
        /**
         * 创建一个新的矩阵，其中包含两个矩阵的乘法。
         * @param matrix1
         * @param matrix2
         * @param result
         */
        Matrix.multiply = function (matrix1, matrix2, result) {
            if (result === void 0) { result = new Matrix(); }
            var m11 = (((matrix1.m11 * matrix2.m11) + (matrix1.m12 * matrix2.m21)) + (matrix1.m13 * matrix2.m31)) + (matrix1.m14 * matrix2.m41);
            var m12 = (((matrix1.m11 * matrix2.m12) + (matrix1.m12 * matrix2.m22)) + (matrix1.m13 * matrix2.m32)) + (matrix1.m14 * matrix2.m42);
            var m13 = (((matrix1.m11 * matrix2.m13) + (matrix1.m12 * matrix2.m23)) + (matrix1.m13 * matrix2.m33)) + (matrix1.m14 * matrix2.m43);
            var m14 = (((matrix1.m11 * matrix2.m14) + (matrix1.m12 * matrix2.m24)) + (matrix1.m13 * matrix2.m34)) + (matrix1.m14 * matrix2.m44);
            var m21 = (((matrix1.m21 * matrix2.m11) + (matrix1.m22 * matrix2.m21)) + (matrix1.m23 * matrix2.m31)) + (matrix1.m24 * matrix2.m41);
            var m22 = (((matrix1.m21 * matrix2.m12) + (matrix1.m22 * matrix2.m22)) + (matrix1.m23 * matrix2.m32)) + (matrix1.m24 * matrix2.m42);
            var m23 = (((matrix1.m21 * matrix2.m13) + (matrix1.m22 * matrix2.m23)) + (matrix1.m23 * matrix2.m33)) + (matrix1.m24 * matrix2.m43);
            var m24 = (((matrix1.m21 * matrix2.m14) + (matrix1.m22 * matrix2.m24)) + (matrix1.m23 * matrix2.m34)) + (matrix1.m24 * matrix2.m44);
            var m31 = (((matrix1.m31 * matrix2.m11) + (matrix1.m32 * matrix2.m21)) + (matrix1.m33 * matrix2.m31)) + (matrix1.m34 * matrix2.m41);
            var m32 = (((matrix1.m31 * matrix2.m12) + (matrix1.m32 * matrix2.m22)) + (matrix1.m33 * matrix2.m32)) + (matrix1.m34 * matrix2.m42);
            var m33 = (((matrix1.m31 * matrix2.m13) + (matrix1.m32 * matrix2.m23)) + (matrix1.m33 * matrix2.m33)) + (matrix1.m34 * matrix2.m43);
            var m34 = (((matrix1.m31 * matrix2.m14) + (matrix1.m32 * matrix2.m24)) + (matrix1.m33 * matrix2.m34)) + (matrix1.m34 * matrix2.m44);
            var m41 = (((matrix1.m41 * matrix2.m11) + (matrix1.m42 * matrix2.m21)) + (matrix1.m43 * matrix2.m31)) + (matrix1.m44 * matrix2.m41);
            var m42 = (((matrix1.m41 * matrix2.m12) + (matrix1.m42 * matrix2.m22)) + (matrix1.m43 * matrix2.m32)) + (matrix1.m44 * matrix2.m42);
            var m43 = (((matrix1.m41 * matrix2.m13) + (matrix1.m42 * matrix2.m23)) + (matrix1.m43 * matrix2.m33)) + (matrix1.m44 * matrix2.m43);
            var m44 = (((matrix1.m41 * matrix2.m14) + (matrix1.m42 * matrix2.m24)) + (matrix1.m43 * matrix2.m34)) + (matrix1.m44 * matrix2.m44);
            result.m11 = m11;
            result.m12 = m12;
            result.m13 = m13;
            result.m14 = m14;
            result.m21 = m21;
            result.m22 = m22;
            result.m23 = m23;
            result.m24 = m24;
            result.m31 = m31;
            result.m32 = m32;
            result.m33 = m33;
            result.m34 = m34;
            result.m41 = m41;
            result.m42 = m42;
            result.m43 = m43;
            result.m44 = m44;
        };
        return Matrix;
    }());
    es.Matrix = Matrix;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * 表示右手3 * 3的浮点矩阵，可以存储平移、缩放和旋转信息。
     */
    var Matrix2D = /** @class */ (function () {
        /**
         * 构建一个矩阵
         * @param m11
         * @param m12
         * @param m21
         * @param m22
         * @param m31
         * @param m32
         */
        function Matrix2D(m11, m12, m21, m22, m31, m32) {
            this.m11 = 0; // x 缩放
            this.m12 = 0;
            this.m21 = 0;
            this.m22 = 0;
            this.m31 = 0;
            this.m32 = 0;
            this.m11 = m11;
            this.m12 = m12;
            this.m21 = m21;
            this.m22 = m22;
            this.m31 = m31;
            this.m32 = m32;
        }
        Object.defineProperty(Matrix2D, "identity", {
            /**
             * 返回标识矩阵
             */
            get: function () {
                return new Matrix2D(1, 0, 0, 1, 0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Matrix2D.prototype, "translation", {
            /**
             * 储存在该矩阵中的位置
             */
            get: function () {
                return new es.Vector2(this.m31, this.m32);
            },
            set: function (value) {
                this.m31 = value.x;
                this.m32 = value.y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Matrix2D.prototype, "rotation", {
            /**
             * 以弧度为单位的旋转，存储在这个矩阵中
             */
            get: function () {
                return Math.atan2(this.m21, this.m11);
            },
            set: function (value) {
                var val1 = Math.cos(value);
                var val2 = Math.sin(value);
                this.m11 = val1;
                this.m12 = val2;
                this.m21 = -val2;
                this.m22 = val1;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Matrix2D.prototype, "rotationDegrees", {
            /**
             * 矩阵中存储的旋转度数
             */
            get: function () {
                return es.MathHelper.toDegrees(this.rotation);
            },
            set: function (value) {
                this.rotation = es.MathHelper.toRadians(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Matrix2D.prototype, "scale", {
            /**
             * 储存在这个矩阵中的缩放
             */
            get: function () {
                return new es.Vector2(this.m11, this.m22);
            },
            set: function (value) {
                this.m11 = value.x;
                this.m22 = value.y;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 创建一个新的围绕Z轴的旋转矩阵2D
         * @param radians
         */
        Matrix2D.createRotation = function (radians) {
            var result = this.identity;
            var val1 = Math.cos(radians);
            var val2 = Math.sin(radians);
            result.m11 = val1;
            result.m12 = val2;
            result.m21 = -val2;
            result.m22 = val1;
            return result;
        };
        /**
         * 创建一个新的缩放矩阵2D
         * @param xScale
         * @param yScale
         */
        Matrix2D.createScale = function (xScale, yScale) {
            var result = this.identity;
            result.m11 = xScale;
            result.m12 = 0;
            result.m21 = 0;
            result.m22 = yScale;
            result.m31 = 0;
            result.m32 = 0;
            return result;
        };
        /**
         * 创建一个新的平移矩阵2D
         * @param xPosition
         * @param yPosition
         */
        Matrix2D.createTranslation = function (xPosition, yPosition) {
            var result = this.identity;
            result.m11 = 1;
            result.m12 = 0;
            result.m21 = 0;
            result.m22 = 1;
            result.m31 = xPosition;
            result.m32 = yPosition;
            return result;
        };
        Matrix2D.invert = function (matrix) {
            var det = 1 / matrix.determinant();
            var result = this.identity;
            result.m11 = matrix.m22 * det;
            result.m12 = -matrix.m12 * det;
            result.m21 = -matrix.m21 * det;
            result.m22 = matrix.m11 * det;
            result.m31 = (matrix.m32 * matrix.m21 - matrix.m31 * matrix.m22) * det;
            result.m32 = -(matrix.m32 * matrix.m11 - matrix.m31 * matrix.m12) * det;
            return result;
        };
        /**
         * 创建一个新的matrix, 它包含两个矩阵的和。
         * @param matrix
         */
        Matrix2D.prototype.add = function (matrix) {
            this.m11 += matrix.m11;
            this.m12 += matrix.m12;
            this.m21 += matrix.m21;
            this.m22 += matrix.m22;
            this.m31 += matrix.m31;
            this.m32 += matrix.m32;
            return this;
        };
        Matrix2D.prototype.substract = function (matrix) {
            this.m11 -= matrix.m11;
            this.m12 -= matrix.m12;
            this.m21 -= matrix.m21;
            this.m22 -= matrix.m22;
            this.m31 -= matrix.m31;
            this.m32 -= matrix.m32;
            return this;
        };
        Matrix2D.prototype.divide = function (matrix) {
            this.m11 /= matrix.m11;
            this.m12 /= matrix.m12;
            this.m21 /= matrix.m21;
            this.m22 /= matrix.m22;
            this.m31 /= matrix.m31;
            this.m32 /= matrix.m32;
            return this;
        };
        Matrix2D.prototype.multiply = function (matrix) {
            var m11 = (this.m11 * matrix.m11) + (this.m12 * matrix.m21);
            var m12 = (this.m11 * matrix.m12) + (this.m12 * matrix.m22);
            var m21 = (this.m21 * matrix.m11) + (this.m22 * matrix.m21);
            var m22 = (this.m21 * matrix.m12) + (this.m22 * matrix.m22);
            var m31 = (this.m31 * matrix.m11) + (this.m32 * matrix.m21) + matrix.m31;
            var m32 = (this.m31 * matrix.m12) + (this.m32 * matrix.m22) + matrix.m32;
            this.m11 = m11;
            this.m12 = m12;
            this.m21 = m21;
            this.m22 = m22;
            this.m31 = m31;
            this.m32 = m32;
            return this;
        };
        Matrix2D.prototype.determinant = function () {
            return this.m11 * this.m22 - this.m12 * this.m21;
        };
        /**
         * 创建一个新的Matrix2D，包含指定矩阵中的线性插值。
         * @param matrix1
         * @param matrix2
         * @param amount
         */
        Matrix2D.lerp = function (matrix1, matrix2, amount) {
            matrix1.m11 = matrix1.m11 + ((matrix2.m11 - matrix1.m11) * amount);
            matrix1.m12 = matrix1.m12 + ((matrix2.m12 - matrix1.m12) * amount);
            matrix1.m21 = matrix1.m21 + ((matrix2.m21 - matrix1.m21) * amount);
            matrix1.m22 = matrix1.m22 + ((matrix2.m22 - matrix1.m22) * amount);
            matrix1.m31 = matrix1.m31 + ((matrix2.m31 - matrix1.m31) * amount);
            matrix1.m32 = matrix1.m32 + ((matrix2.m32 - matrix1.m32) * amount);
            return matrix1;
        };
        /**
         * 交换矩阵的行和列
         * @param matrix
         */
        Matrix2D.transpose = function (matrix) {
            var ret = this.identity;
            ret.m11 = matrix.m11;
            ret.m12 = matrix.m21;
            ret.m21 = matrix.m12;
            ret.m22 = matrix.m22;
            ret.m31 = 0;
            ret.m32 = 0;
            return ret;
        };
        Matrix2D.prototype.mutiplyTranslation = function (x, y) {
            var trans = Matrix2D.createTranslation(x, y);
            return es.MatrixHelper.mutiply(this, trans);
        };
        /**
         * 比较当前实例是否等于指定的Matrix2D
         * @param other
         */
        Matrix2D.prototype.equals = function (other) {
            return this == other;
        };
        Matrix2D.toMatrix = function (mat) {
            var matrix = new es.Matrix();
            matrix.m11 = mat.m11;
            matrix.m12 = mat.m12;
            matrix.m13 = 0;
            matrix.m14 = 0;
            matrix.m21 = mat.m21;
            matrix.m22 = mat.m22;
            matrix.m23 = 0;
            matrix.m24 = 0;
            matrix.m31 = 0;
            matrix.m32 = 0;
            matrix.m33 = 1;
            matrix.m34 = 0;
            matrix.m41 = mat.m31;
            matrix.m42 = mat.m32;
            matrix.m43 = 0;
            matrix.m44 = 1;
            return matrix;
        };
        Matrix2D.prototype.toString = function () {
            return "{m11:" + this.m11 + " m12:" + this.m12 + " m21:" + this.m21 + " m22:" + this.m22 + " m31:" + this.m31 + " m32:" + this.m32 + "}";
        };
        return Matrix2D;
    }());
    es.Matrix2D = Matrix2D;
})(es || (es = {}));
var es;
(function (es) {
    var MatrixHelper = /** @class */ (function () {
        function MatrixHelper() {
        }
        /**
         * 创建一个新的Matrix2D，其中包含两个矩阵的和
         * @param matrix1
         * @param matrix2
         */
        MatrixHelper.add = function (matrix1, matrix2) {
            var result = es.Matrix2D.identity;
            result.m11 = matrix1.m11 + matrix2.m11;
            result.m12 = matrix1.m12 + matrix2.m12;
            result.m21 = matrix1.m21 + matrix2.m21;
            result.m22 = matrix1.m22 + matrix2.m22;
            result.m31 = matrix1.m31 + matrix2.m31;
            result.m32 = matrix1.m32 + matrix2.m32;
            return result;
        };
        /**
         * 将一个Matrix2D的元素除以另一个矩阵的元素
         * @param matrix1
         * @param matrix2
         */
        MatrixHelper.divide = function (matrix1, matrix2) {
            var result = es.Matrix2D.identity;
            result.m11 = matrix1.m11 / matrix2.m11;
            result.m12 = matrix1.m12 / matrix2.m12;
            result.m21 = matrix1.m21 / matrix2.m21;
            result.m22 = matrix1.m22 / matrix2.m22;
            result.m31 = matrix1.m31 / matrix2.m31;
            result.m32 = matrix1.m32 / matrix2.m32;
            return result;
        };
        /**
         * 创建一个新的Matrix2D，包含两个矩阵的乘法
         * @param matrix1
         * @param matrix2
         */
        MatrixHelper.mutiply = function (matrix1, matrix2) {
            var result = es.Matrix2D.identity;
            if (matrix2 instanceof es.Matrix2D) {
                var m11 = (matrix1.m11 * matrix2.m11) + (matrix1.m12 * matrix2.m21);
                var m12 = (matrix2.m11 * matrix2.m12) + (matrix1.m12 * matrix2.m22);
                var m21 = (matrix1.m21 * matrix2.m11) + (matrix1.m22 * matrix2.m21);
                var m22 = (matrix1.m21 * matrix2.m12) + (matrix1.m22 * matrix2.m22);
                var m31 = (matrix1.m31 * matrix2.m11) + (matrix1.m32 * matrix2.m21) + matrix2.m31;
                var m32 = (matrix1.m31 * matrix2.m12) + (matrix1.m32 * matrix2.m22) + matrix2.m32;
                result.m11 = m11;
                result.m12 = m12;
                result.m21 = m21;
                result.m22 = m22;
                result.m31 = m31;
                result.m32 = m32;
            }
            else if (typeof matrix2 == "number") {
                result.m11 = matrix1.m11 * matrix2;
                result.m12 = matrix1.m12 * matrix2;
                result.m21 = matrix1.m21 * matrix2;
                result.m22 = matrix1.m22 * matrix2;
                result.m31 = matrix1.m31 * matrix2;
                result.m32 = matrix1.m32 * matrix2;
            }
            return result;
        };
        /**
         * 创建一个新的Matrix2D，包含一个矩阵与另一个矩阵的减法。
         * @param matrix1
         * @param matrix2
         */
        MatrixHelper.subtract = function (matrix1, matrix2) {
            var result = es.Matrix2D.identity;
            result.m11 = matrix1.m11 - matrix2.m11;
            result.m12 = matrix1.m12 - matrix2.m12;
            result.m21 = matrix1.m21 - matrix2.m21;
            result.m22 = matrix1.m22 - matrix2.m22;
            result.m31 = matrix1.m31 - matrix2.m31;
            result.m32 = matrix1.m32 - matrix2.m32;
            return result;
        };
        return MatrixHelper;
    }());
    es.MatrixHelper = MatrixHelper;
})(es || (es = {}));
var es;
(function (es) {
    var Rectangle = /** @class */ (function () {
        /**
         * 创建一个新的Rectanglestruct实例，指定位置、宽度和高度。
         * @param x 创建的矩形的左上角的X坐标
         * @param y 创建的矩形的左上角的y坐标
         * @param width 创建的矩形的宽度
         * @param height 创建的矩形的高度
         */
        function Rectangle(x, y, width, height) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (width === void 0) { width = 0; }
            if (height === void 0) { height = 0; }
            /**
             * 该矩形的左上角的x坐标
             */
            this.x = 0;
            /**
             * 该矩形的左上角的y坐标
             */
            this.y = 0;
            /**
             * 该矩形的宽度
             */
            this.width = 0;
            /**
             * 该矩形的高度
             */
            this.height = 0;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        Object.defineProperty(Rectangle, "empty", {
            /**
             * 返回X=0, Y=0, Width=0, Height=0的矩形
             */
            get: function () {
                return new Rectangle();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle, "maxRect", {
            /**
             * 返回一个Number.Min/Max值的矩形
             */
            get: function () {
                return new Rectangle(Number.MIN_VALUE / 2, Number.MIN_VALUE / 2, Number.MAX_VALUE, Number.MAX_VALUE);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "left", {
            /**
             * 返回此矩形左边缘的X坐标
             */
            get: function () {
                return this.x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "right", {
            /**
             * 返回此矩形右边缘的X坐标
             */
            get: function () {
                return this.x + this.width;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "top", {
            /**
             * 返回此矩形顶边的y坐标
             */
            get: function () {
                return this.y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "bottom", {
            /**
             * 返回此矩形底边的y坐标
             */
            get: function () {
                return this.y + this.height;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "max", {
            /**
             * 获取矩形的最大点，即右下角
             */
            get: function () {
                return new es.Vector2(this.right, this.bottom);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 这个矩形的宽和高是否为0，位置是否为（0，0）
         */
        Rectangle.prototype.isEmpty = function () {
            return ((((this.width == 0) && (this.height == 0)) && (this.x == 0)) && (this.y == 0));
        };
        Object.defineProperty(Rectangle.prototype, "location", {
            /** 这个矩形的左上角坐标 */
            get: function () {
                return new es.Vector2(this.x, this.y);
            },
            set: function (value) {
                this.x = value.x;
                this.y = value.y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "size", {
            /**
             * 这个矩形的宽-高坐标
             */
            get: function () {
                return new es.Vector2(this.width, this.height);
            },
            set: function (value) {
                this.width = value.x;
                this.height = value.y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "center", {
            /**
             * 位于这个矩形中心的一个点
             * 如果 "宽度 "或 "高度 "是奇数，则中心点将向下舍入
             */
            get: function () {
                return new es.Vector2(this.x + (this.width / 2), this.y + (this.height / 2));
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 创建一个给定最小/最大点（左上角，右下角）的矩形
         * @param minX
         * @param minY
         * @param maxX
         * @param maxY
         */
        Rectangle.fromMinMax = function (minX, minY, maxX, maxY) {
            return new Rectangle(minX, minY, maxX - minX, maxY - minY);
        };
        /**
         * 给定多边形的点，计算边界
         * @param points
         * @returns 来自多边形的点
         */
        Rectangle.rectEncompassingPoints = function (points) {
            // 我们需要求出x/y的最小值/最大值
            var minX = Number.POSITIVE_INFINITY;
            var minY = Number.POSITIVE_INFINITY;
            var maxX = Number.NEGATIVE_INFINITY;
            var maxY = Number.NEGATIVE_INFINITY;
            for (var i = 0; i < points.length; i++) {
                var pt = points[i];
                if (pt.x < minX)
                    minX = pt.x;
                if (pt.x > maxX)
                    maxX = pt.x;
                if (pt.y < minY)
                    minY = pt.y;
                if (pt.y > maxY)
                    maxY = pt.y;
            }
            return this.fromMinMax(minX, minY, maxX, maxY);
        };
        /**
         * 获取指定边缘的位置
         * @param edge
         */
        Rectangle.prototype.getSide = function (edge) {
            switch (edge) {
                case es.Edge.top:
                    return this.top;
                case es.Edge.bottom:
                    return this.bottom;
                case es.Edge.left:
                    return this.left;
                case es.Edge.right:
                    return this.right;
                default:
                    throw new Error("Argument Out Of Range");
            }
        };
        /**
         * 获取所提供的坐标是否在这个矩形的范围内
         * @param x 检查封堵点的X坐标
         * @param y 检查封堵点的Y坐标
         */
        Rectangle.prototype.contains = function (x, y) {
            return ((((this.x <= x) && (x < (this.x + this.width))) &&
                (this.y <= y)) && (y < (this.y + this.height)));
        };
        /**
         * 按指定的水平和垂直方向调整此矩形的边缘
         * @param horizontalAmount 调整左、右边缘的值
         * @param verticalAmount 调整上、下边缘的值
         */
        Rectangle.prototype.inflate = function (horizontalAmount, verticalAmount) {
            this.x -= horizontalAmount;
            this.y -= verticalAmount;
            this.width += horizontalAmount * 2;
            this.height += verticalAmount * 2;
        };
        /**
         * 获取其他矩形是否与这个矩形相交
         * @param value 另一个用于测试的矩形
         */
        Rectangle.prototype.intersects = function (value) {
            return value.left < this.right &&
                this.left < value.right &&
                value.top < this.bottom &&
                this.top < value.bottom;
        };
        Rectangle.prototype.rayIntersects = function (ray, distance) {
            distance.value = 0;
            var maxValue = Number.MAX_VALUE;
            if (Math.abs(ray.direction.x) < 1E-06) {
                if ((ray.start.x < this.x) || (ray.start.x > this.x + this.width))
                    return false;
            }
            else {
                var num11 = 1 / ray.direction.x;
                var num8 = (this.x - ray.start.x) * num11;
                var num7 = (this.x + this.width - ray.start.x) * num11;
                if (num8 > num7) {
                    var num14 = num8;
                    num8 = num7;
                    num7 = num14;
                }
                distance.value = Math.max(num8, distance.value);
                maxValue = Math.min(num7, maxValue);
                if (distance.value > maxValue)
                    return false;
            }
            if (Math.abs(ray.direction.y) < 1E-06) {
                if ((ray.start.y < this.y) || (ray.start.y > this.y + this.height))
                    return false;
            }
            else {
                var num10 = 1 / ray.direction.y;
                var num6 = (this.y - ray.start.y) * num10;
                var num5 = (this.y + this.height - ray.start.y) * num10;
                if (num6 > num5) {
                    var num13 = num6;
                    num6 = num5;
                    num5 = num13;
                }
                distance.value = Math.max(num6, distance.value);
                maxValue = Math.max(num5, maxValue);
                if (distance.value > maxValue)
                    return false;
            }
            return true;
        };
        /**
         * 获取所提供的矩形是否在此矩形的边界内
         * @param value
         */
        Rectangle.prototype.containsRect = function (value) {
            return ((((this.x <= value.x) && (value.x < (this.x + this.width))) &&
                (this.y <= value.y)) &&
                (value.y < (this.y + this.height)));
        };
        Rectangle.prototype.getHalfSize = function () {
            return new es.Vector2(this.width * 0.5, this.height * 0.5);
        };
        Rectangle.prototype.getClosestPointOnBoundsToOrigin = function () {
            var max = this.max;
            var minDist = Math.abs(this.location.x);
            var boundsPoint = new es.Vector2(this.location.x, 0);
            if (Math.abs(max.x) < minDist) {
                minDist = Math.abs(max.x);
                boundsPoint.x = max.x;
                boundsPoint.y = 0;
            }
            if (Math.abs(max.y) < minDist) {
                minDist = Math.abs(max.y);
                boundsPoint.x = 0;
                boundsPoint.y = max.y;
            }
            if (Math.abs(this.location.y) < minDist) {
                minDist = Math.abs(this.location.y);
                boundsPoint.x = 0;
                boundsPoint.y = this.location.y;
            }
            return boundsPoint;
        };
        /**
         * 返回离给定点最近的点
         * @param point 矩形上离点最近的点
         */
        Rectangle.prototype.getClosestPointOnRectangleToPoint = function (point) {
            // 对于每条轴，如果点在框外，就把它限制在框内，否则就不要管它
            var res = new es.Vector2();
            res.x = es.MathHelper.clamp(point.x, this.left, this.right);
            res.y = es.MathHelper.clamp(point.y, this.top, this.bottom);
            return res;
        };
        /**
         * 获取矩形边界上与给定点最近的点
         * @param point
         * @param edgeNormal
         * @returns 矩形边框上离点最近的点
         */
        Rectangle.prototype.getClosestPointOnRectangleBorderToPoint = function (point, edgeNormal) {
            // 对于每条轴，如果点在框外，就把它限制在框内，否则就不要管它
            var res = new es.Vector2();
            res.x = es.MathHelper.clamp(point.x, this.left, this.right);
            res.y = es.MathHelper.clamp(point.y, this.top, this.bottom);
            // 如果点在矩形内，我们需要将res推到边界上，因为它将在矩形内
            if (this.contains(res.x, res.y)) {
                var dl = res.x - this.left;
                var dr = this.right - res.x;
                var dt = res.y - this.top;
                var db = this.bottom - res.y;
                var min = Math.min(dl, dr, dt, db);
                if (min == dt) {
                    res.y = this.top;
                    edgeNormal.y = -1;
                }
                else if (min == db) {
                    res.y = this.bottom;
                    edgeNormal.y = 1;
                }
                else if (min == dl) {
                    res.x = this.left;
                    edgeNormal.x = -1;
                }
                else {
                    res.x = this.right;
                    edgeNormal.x = 1;
                }
            }
            else {
                if (res.x == this.left)
                    edgeNormal.x = -1;
                if (res.x == this.right)
                    edgeNormal.x = 1;
                if (res.y == this.top)
                    edgeNormal.y = -1;
                if (res.y == this.bottom)
                    edgeNormal.y = 1;
            }
            return res;
        };
        /**
         * 创建一个新的RectangleF，该RectangleF包含两个其他矩形的重叠区域
         * @param value1
         * @param value2
         * @returns 将两个矩形的重叠区域作为输出参数
         */
        Rectangle.intersect = function (value1, value2) {
            if (value1.intersects(value2)) {
                var right_side = Math.min(value1.x + value1.width, value2.x + value2.width);
                var left_side = Math.max(value1.x, value2.x);
                var top_side = Math.max(value1.y, value2.y);
                var bottom_side = Math.min(value1.y + value1.height, value2.y + value2.height);
                return new Rectangle(left_side, top_side, right_side - left_side, bottom_side - top_side);
            }
            else {
                return new Rectangle(0, 0, 0, 0);
            }
        };
        /**
         * 改变这个矩形的位置
         * @param offsetX 要添加到这个矩形的X坐标
         * @param offsetY 要添加到这个矩形的y坐标
         */
        Rectangle.prototype.offset = function (offsetX, offsetY) {
            this.x += offsetX;
            this.y += offsetY;
        };
        /**
         * 创建一个完全包含两个其他矩形的新矩形
         * @param value1
         * @param value2
         */
        Rectangle.union = function (value1, value2) {
            var x = Math.min(value1.x, value2.x);
            var y = Math.min(value1.y, value2.y);
            return new Rectangle(x, y, Math.max(value1.right, value2.right) - x, Math.max(value1.bottom, value2.bottom) - y);
        };
        /**
         * 在矩形重叠的地方创建一个新的矩形
         * @param value1
         * @param value2
         */
        Rectangle.overlap = function (value1, value2) {
            var x = Math.max(value1.x, value2.x, 0);
            var y = Math.max(value1.y, value2.y, 0);
            return new Rectangle(x, y, Math.max(Math.min(value1.right, value2.right) - x, 0), Math.max(Math.min(value1.bottom, value2.bottom) - y, 0));
        };
        Rectangle.prototype.calculateBounds = function (parentPosition, position, origin, scale, rotation, width, height) {
            if (rotation == 0) {
                this.x = parentPosition.x + position.x - origin.x * scale.x;
                this.y = parentPosition.y + position.y - origin.y * scale.y;
                this.width = width * scale.x;
                this.height = height * scale.y;
            }
            else {
                // 我们需要找到我们的绝对最小/最大值，并据此创建边界
                var worldPosX = parentPosition.x + position.x;
                var worldPosY = parentPosition.y + position.y;
                // 考虑到原点，将参考点设置为世界参考
                this._transformMat = es.Matrix2D.createTranslation(-worldPosX - origin.x, -worldPosY - origin.y);
                this._tempMat = es.Matrix2D.createScale(scale.x, scale.y);
                this._transformMat = this._transformMat.multiply(this._tempMat);
                this._tempMat = es.Matrix2D.createRotation(rotation);
                this._transformMat = this._transformMat.multiply(this._tempMat);
                this._tempMat = es.Matrix2D.createTranslation(worldPosX, worldPosY);
                this._transformMat = this._transformMat.multiply(this._tempMat);
                // TODO: 我们可以把世界变换留在矩阵中，避免在世界空间中得到所有的四个角
                var topLeft = new es.Vector2(worldPosX, worldPosY);
                var topRight = new es.Vector2(worldPosX + width, worldPosY);
                var bottomLeft = new es.Vector2(worldPosX, worldPosY + height);
                var bottomRight = new es.Vector2(worldPosX + width, worldPosY + height);
                es.Vector2Ext.transformR(topLeft, this._transformMat, topLeft);
                es.Vector2Ext.transformR(topRight, this._transformMat, topRight);
                es.Vector2Ext.transformR(bottomLeft, this._transformMat, bottomLeft);
                es.Vector2Ext.transformR(bottomRight, this._transformMat, bottomRight);
                // 找出最小值和最大值，这样我们就可以计算出我们的边界框。
                var minX = Math.min(topLeft.x, bottomRight.x, topRight.x, bottomLeft.x);
                var maxX = Math.max(topLeft.x, bottomRight.x, topRight.x, bottomLeft.x);
                var minY = Math.min(topLeft.y, bottomRight.y, topRight.y, bottomLeft.y);
                var maxY = Math.max(topLeft.y, bottomRight.y, topRight.y, bottomLeft.y);
                this.location = new es.Vector2(minX, minY);
                this.width = maxX - minX;
                this.height = maxY - minY;
            }
        };
        /**
         * 返回一个横跨当前矩形和提供的三角形位置的矩形
         * @param deltaX
         * @param deltaY
         */
        Rectangle.prototype.getSweptBroadphaseBounds = function (deltaX, deltaY) {
            var broadphasebox = Rectangle.empty;
            broadphasebox.x = deltaX > 0 ? this.x : this.x + deltaX;
            broadphasebox.y = deltaY > 0 ? this.y : this.y + deltaY;
            broadphasebox.width = deltaX > 0 ? deltaX + this.width : this.width - deltaX;
            broadphasebox.height = deltaY > 0 ? deltaY + this.height : this.height - deltaY;
            return broadphasebox;
        };
        /**
         * 如果发生碰撞，返回true
         * moveX和moveY将返回b1为避免碰撞而必须移动的移动量
         * @param other
         * @param moveX
         * @param moveY
         */
        Rectangle.prototype.collisionCheck = function (other, moveX, moveY) {
            moveX.value = moveY.value = 0;
            var l = other.x - (this.x + this.width);
            var r = (other.x + other.width) - this.x;
            var t = (other.y - (this.y + this.height));
            var b = (other.y + other.height) - this.y;
            // 检验是否有碰撞
            if (l > 0 || r < 0 || t > 0 || b < 0)
                return false;
            // 求两边的偏移量
            moveX.value = Math.abs(l) < r ? l : r;
            moveY.value = Math.abs(t) < b ? t : b;
            // 只使用最小的偏移量
            if (Math.abs(moveX.value) < Math.abs(moveY.value))
                moveY.value = 0;
            else
                moveX.value = 0;
            return true;
        };
        /**
         * 计算两个矩形之间有符号的交点深度
         * @param rectA
         * @param rectB
         * @returns 两个相交的矩形之间的重叠量。
         * 这些深度值可以是负值，取决于矩形/相交的哪些边。
         * 这允许调用者确定正确的推送对象的方向，以解决碰撞问题。
         * 如果矩形不相交，则返回Vector2.Zero
         */
        Rectangle.getIntersectionDepth = function (rectA, rectB) {
            // 计算半尺寸
            var halfWidthA = rectA.width / 2;
            var halfHeightA = rectA.height / 2;
            var halfWidthB = rectB.width / 2;
            var halfHeightB = rectB.height / 2;
            // 计算中心
            var centerA = new es.Vector2(rectA.left + halfWidthA, rectA.top + halfHeightA);
            var centerB = new es.Vector2(rectB.left + halfWidthB, rectB.top + halfHeightB);
            // 计算当前中心间的距离和最小非相交距离
            var distanceX = centerA.x - centerB.x;
            var distanceY = centerA.y - centerB.y;
            var minDistanceX = halfWidthA + halfWidthB;
            var minDistanceY = halfHeightA + halfHeightB;
            // 如果我们根本不相交，则返回(0，0)
            if (Math.abs(distanceX) >= minDistanceX || Math.abs(distanceY) >= minDistanceY)
                return es.Vector2.ZERO;
            // 计算并返回交叉点深度
            var depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
            var depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;
            return new es.Vector2(depthX, depthY);
        };
        /**
         * 比较当前实例是否等于指定的矩形
         * @param other
         */
        Rectangle.prototype.equals = function (other) {
            return this === other;
        };
        /**
         * 获取这个矩形的哈希码
         */
        Rectangle.prototype.getHashCode = function () {
            return (this.x ^ this.y ^ this.width ^ this.height);
        };
        Rectangle.prototype.clone = function () {
            return new Rectangle(this.x, this.y, this.width, this.height);
        };
        return Rectangle;
    }());
    es.Rectangle = Rectangle;
})(es || (es = {}));
var es;
(function (es) {
    var SortUtils = /** @class */ (function () {
        function SortUtils() {
        }
        //defaullt compare function
        SortUtils.defaultCompareFn = function (a, b) {
            return a - b;
        };
        /**
         * Sort array elements，
         * @param list any type array
         * @param compareFn Numerical comparison is used by default, { -1, 0 , 1 }
         */
        SortUtils.sort = function (list, compareFn) {
            var fn = compareFn || SortUtils.defaultCompareFn;
            SortUtils._sort(list, 0, list.length - 1, fn);
        };
        //quick_sort
        SortUtils._sort = function (q, l, r, fn) {
            if (l >= r)
                return;
            var x = q[l], i = l - 1, j = r + 1;
            while (i < j) {
                do
                    i++;
                while (fn(q[i], x) < 0);
                do
                    j--;
                while (fn(q[j], x) > 0);
                if (i < j) {
                    var temp = q[i];
                    q[i] = q[j];
                    q[j] = temp;
                }
            }
            SortUtils._sort(q, l, j, fn);
            SortUtils._sort(q, j + 1, r, fn);
        };
        return SortUtils;
    }());
    es.SortUtils = SortUtils;
})(es || (es = {}));
var es;
(function (es) {
    /** 2d 向量 */
    var Vector2 = /** @class */ (function () {
        /**
         * 从两个值构造一个带有X和Y的二维向量。
         * @param x 二维空间中的x坐标
         * @param y 二维空间的y坐标
         */
        function Vector2(x, y) {
            this.x = 0;
            this.y = 0;
            this.x = x ? x : 0;
            this.y = y != undefined ? y : this.x;
        }
        Vector2.FromIVector2 = function (ivec2data) {
            return new Vector2(ivec2data.x, ivec2data.y);
        };
        Object.defineProperty(Vector2, "ZERO", {
            get: function () {
                return new Vector2(0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector2, "ONE", {
            get: function () {
                return new Vector2(1, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector2, "RIGHT", {
            get: function () {
                return new Vector2(1, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector2, "UP", {
            get: function () {
                return new Vector2(0, 1);
            },
            enumerable: true,
            configurable: true
        });
        /**
         *
         * @param value1
         * @param value2
         */
        Vector2.add = function (value1, value2) {
            var result = Vector2.ZERO;
            result.x = value1.x + value2.x;
            result.y = value1.y + value2.y;
            return result;
        };
        /**
         *
         * @param value1
         * @param value2
         */
        Vector2.divide = function (value1, value2) {
            var result = Vector2.ZERO;
            result.x = value1.x / value2.x;
            result.y = value1.y / value2.y;
            return result;
        };
        /**
         *
         * @param value1
         * @param value2
         */
        Vector2.multiply = function (value1, value2) {
            var result = new Vector2(0, 0);
            result.x = value1.x * value2.x;
            result.y = value1.y * value2.y;
            return result;
        };
        /**
         *
         * @param value1
         * @param value2
         */
        Vector2.subtract = function (value1, value2) {
            var result = new Vector2(0, 0);
            result.x = value1.x - value2.x;
            result.y = value1.y - value2.y;
            return result;
        };
        /**
         * 创建一个新的Vector2
         * 它包含来自另一个向量的标准化值。
         * @param value
         */
        Vector2.normalize = function (value) {
            var nValue = new Vector2(value.x, value.y);
            var val = 1 / Math.sqrt((nValue.x * nValue.x) + (nValue.y * nValue.y));
            nValue.x *= val;
            nValue.y *= val;
            return nValue;
        };
        /**
         * 返回两个向量的点积
         * @param value1
         * @param value2
         */
        Vector2.dot = function (value1, value2) {
            return (value1.x * value2.x) + (value1.y * value2.y);
        };
        /**
         * 向量缩放
         * @param a 原向量
         * @param r 比例
         * @returns 返回缩放后的原向量
         */
        Vector2.scale = function (a, r) {
            a.x *= r;
            a.y *= r;
            return a;
        };
        /**
         * 自身缩放
         * @param r 比例
         * @returns 返回缩放后的this
         */
        Vector2.prototype.scale = function (r) {
            return Vector2.scale(this, r);
        };
        /**
         * 计算反弹向量
         * @param inDirection 入射向量
         * @param inNormal 法线
         * @param result 返回反弹向量
         * @returns 反弹向量
         */
        Vector2.reflect = function (inDirection, inNormal, result) {
            result = result || new Vector2();
            Vector2.clonTo(inNormal, result);
            inDirection.subtract(result.scale(2 * Vector2.dot(inDirection, result)));
            Vector2.clonTo(inDirection, result);
            return result;
        };
        /**
         * src赋值给dest
         * @param src
         * @param dest
         */
        Vector2.clonTo = function (src, dest) {
            dest.x = src.x;
            dest.y = src.y;
        };
        /**
         * 返回两个向量之间距离的平方
         * @param value1
         * @param value2
         */
        Vector2.distanceSquared = function (value1, value2) {
            var v1 = value1.x - value2.x, v2 = value1.y - value2.y;
            return (v1 * v1) + (v2 * v2);
        };
        /**
         * 将指定的值限制在一个范围内
         * @param value1
         * @param min
         * @param max
         */
        Vector2.clamp = function (value1, min, max) {
            return new Vector2(es.MathHelper.clamp(value1.x, min.x, max.x), es.MathHelper.clamp(value1.y, min.y, max.y));
        };
        /**
         * 创建一个新的Vector2，其中包含指定向量的线性插值
         * @param value1 第一个向量
         * @param value2 第二个向量
         * @param amount 加权值(0.0-1.0之间)
         * @returns 指定向量的线性插值结果
         */
        Vector2.lerp = function (value1, value2, amount) {
            return new Vector2(es.MathHelper.lerp(value1.x, value2.x, amount), es.MathHelper.lerp(value1.y, value2.y, amount));
        };
        /**
         * 创建一个新的Vector2，该Vector2包含了通过指定的Matrix进行的二维向量变换。
         * @param position
         * @param matrix
         */
        Vector2.transform = function (position, matrix) {
            return new Vector2((position.x * matrix.m11) + (position.y * matrix.m21) + matrix.m31, (position.x * matrix.m12) + (position.y * matrix.m22) + matrix.m32);
        };
        /**
         * 返回两个向量之间的距离
         * @param value1
         * @param value2
         * @returns 两个向量之间的距离
         */
        Vector2.distance = function (value1, value2) {
            var v1 = value1.x - value2.x, v2 = value1.y - value2.y;
            return Math.sqrt((v1 * v1) + (v2 * v2));
        };
        /**
         * 返回两个向量之间的角度，单位是度数
         * @param from
         * @param to
         */
        Vector2.angle = function (from, to) {
            from = Vector2.normalize(from);
            to = Vector2.normalize(to);
            return Math.acos(es.MathHelper.clamp(Vector2.dot(from, to), -1, 1)) * es.MathHelper.Rad2Deg;
        };
        /**
         * 创建一个包含指定向量反转的新Vector2
         * @param value
         * @returns 矢量反演的结果
         */
        Vector2.negate = function (value) {
            value.x = -value.x;
            value.y = -value.y;
            return value;
        };
        /**
         *
         * @param value
         */
        Vector2.prototype.add = function (value) {
            this.x += value.x;
            this.y += value.y;
            return this;
        };
        /**
         *
         * @param value
         */
        Vector2.prototype.divide = function (value) {
            this.x /= value.x;
            this.y /= value.y;
            return this;
        };
        /**
         *
         * @param value
         */
        Vector2.prototype.multiply = function (value) {
            this.x *= value.x;
            this.y *= value.y;
            return this;
        };
        /**
         * 从当前Vector2减去一个Vector2
         * @param value 要减去的Vector2
         * @returns 当前Vector2
         */
        Vector2.prototype.subtract = function (value) {
            this.x -= value.x;
            this.y -= value.y;
            return this;
        };
        /**
         * 将这个Vector2变成一个方向相同的单位向量
         */
        Vector2.prototype.normalize = function () {
            var val = 1 / Math.sqrt((this.x * this.x) + (this.y * this.y));
            this.x *= val;
            this.y *= val;
        };
        /** 返回它的长度 */
        Vector2.prototype.length = function () {
            return Math.sqrt((this.x * this.x) + (this.y * this.y));
        };
        /**
         * 返回该Vector2的平方长度
         * @returns 这个Vector2的平方长度
         */
        Vector2.prototype.lengthSquared = function () {
            return (this.x * this.x) + (this.y * this.y);
        };
        /**
         * 四舍五入X和Y值
         */
        Vector2.prototype.round = function () {
            return new Vector2(Math.round(this.x), Math.round(this.y));
        };
        /**
         * 返回以自己为中心点的左右角，单位为度
         * @param left
         * @param right
         */
        Vector2.prototype.angleBetween = function (left, right) {
            var one = Vector2.subtract(left, this);
            var two = Vector2.subtract(right, this);
            return es.Vector2Ext.angle(one, two);
        };
        /**
         * 比较当前实例是否等于指定的对象
         * @param other 要比较的对象
         * @returns 如果实例相同true 否则false
         */
        Vector2.prototype.equals = function (other) {
            return other.x == this.x && other.y == this.y;
        };
        Vector2.prototype.equalf = function (other, precision) {
            if (precision === void 0) { precision = 1e-4; }
            return Math.abs(other.x - this.x) <= precision && Math.abs(other.y - this.y) <= precision;
        };
        Vector2.prototype.clone = function () {
            return new Vector2(this.x, this.y);
        };
        return Vector2;
    }());
    es.Vector2 = Vector2;
})(es || (es = {}));
var es;
(function (es) {
    var Vector2Ext = /** @class */ (function () {
        function Vector2Ext() {
        }
        /**
         * 检查三角形是CCW还是CW
         * @param a
         * @param center
         * @param c
         */
        Vector2Ext.isTriangleCCW = function (a, center, c) {
            return this.cross(es.Vector2.subtract(center, a), es.Vector2.subtract(c, center)) < 0;
        };
        Vector2Ext.halfVector = function () {
            return new es.Vector2(0.5, 0.5);
        };
        /**
         * 计算二维伪叉乘点(Perp(u)， v)
         * @param u
         * @param v
         */
        Vector2Ext.cross = function (u, v) {
            return u.y * v.x - u.x * v.y;
        };
        /**
         * 返回垂直于传入向量的向量
         * @param first
         * @param second
         */
        Vector2Ext.perpendicular = function (first, second) {
            return new es.Vector2(-1 * (second.y - first.y), second.x - first.x);
        };
        /**
         * 将x/y值翻转，并将y反转，得到垂直于x/y的值
         * @param original
         */
        Vector2Ext.perpendicularFlip = function (original) {
            return new es.Vector2(-original.y, original.x);
        };
        /**
         * 返回两个向量之间的角度，单位为度
         * @param from
         * @param to
         */
        Vector2Ext.angle = function (from, to) {
            this.normalize(from);
            this.normalize(to);
            return Math.acos(es.MathHelper.clamp(es.Vector2.dot(from, to), -1, 1)) * es.MathHelper.Rad2Deg;
        };
        /**
         * 给定两条直线(ab和cd)，求交点
         * @param a
         * @param b
         * @param c
         * @param d
         * @param intersection
         */
        Vector2Ext.getRayIntersection = function (a, b, c, d, intersection) {
            if (intersection === void 0) { intersection = new es.Vector2(); }
            var dy1 = b.y - a.y;
            var dx1 = b.x - a.x;
            var dy2 = d.y - c.y;
            var dx2 = d.x - c.x;
            if (dy1 * dx2 == dy2 * dx1) {
                intersection.x = Number.NaN;
                intersection.y = Number.NaN;
                return false;
            }
            var x = ((c.y - a.y) * dx1 * dx2 + dy1 * dx2 * a.x - dy2 * dx1 * c.x) / (dy1 * dx2 - dy2 * dx1);
            var y = a.y + (dy1 / dx1) * (x - a.x);
            intersection.x = x;
            intersection.y = y;
            return true;
        };
        /**
         * Vector2的临时解决方案
         * 标准化把向量弄乱了
         * @param vec
         */
        Vector2Ext.normalize = function (vec) {
            var magnitude = Math.sqrt((vec.x * vec.x) + (vec.y * vec.y));
            if (magnitude > es.MathHelper.Epsilon) {
                vec.divide(new es.Vector2(magnitude));
            }
            else {
                vec.x = vec.y = 0;
            }
        };
        /**
         * 通过指定的矩阵对Vector2的数组中的向量应用变换，并将结果放置在另一个数组中。
         * @param sourceArray
         * @param sourceIndex
         * @param matrix
         * @param destinationArray
         * @param destinationIndex
         * @param length
         */
        Vector2Ext.transformA = function (sourceArray, sourceIndex, matrix, destinationArray, destinationIndex, length) {
            for (var i = 0; i < length; i++) {
                var position = sourceArray[sourceIndex + i];
                var destination = destinationArray[destinationIndex + i];
                destination.x = (position.x * matrix.m11) + (position.y * matrix.m21) + matrix.m31;
                destination.y = (position.x * matrix.m12) + (position.y * matrix.m22) + matrix.m32;
                destinationArray[destinationIndex + i] = destination;
            }
        };
        /**
         * 创建一个新的Vector2，该Vector2包含了通过指定的Matrix进行的二维向量变换
         * @param position
         * @param matrix
         * @param result
         */
        Vector2Ext.transformR = function (position, matrix, result) {
            if (result === void 0) { result = new es.Vector2(); }
            var x = (position.x * matrix.m11) + (position.y * matrix.m21) + matrix.m31;
            var y = (position.x * matrix.m12) + (position.y * matrix.m22) + matrix.m32;
            result.x = x;
            result.y = y;
        };
        /**
         * 通过指定的矩阵对Vector2的数组中的所有向量应用变换，并将结果放到另一个数组中。
         * @param sourceArray
         * @param matrix
         * @param destinationArray
         */
        Vector2Ext.transform = function (sourceArray, matrix, destinationArray) {
            this.transformA(sourceArray, 0, matrix, destinationArray, 0, sourceArray.length);
        };
        Vector2Ext.round = function (vec) {
            return new es.Vector2(Math.round(vec.x), Math.round(vec.y));
        };
        return Vector2Ext;
    }());
    es.Vector2Ext = Vector2Ext;
})(es || (es = {}));
var es;
(function (es) {
    var IMono = /** @class */ (function () {
        function IMono() {
        }
        IMono.prototype.enableGUI = function () {
            this.disableGUI();
            es.Debug.debug && Laya.timer.loop(1, this, this.OnGUI);
        };
        IMono.prototype.disableGUI = function () {
            es.Debug.debug && Laya.timer.clear(this, this.OnGUI);
        };
        IMono.prototype.Print = function (any) {
            es.Debug.log(any);
        };
        return IMono;
    }());
    es.IMono = IMono;
})(es || (es = {}));
var es;
(function (es) {
    var ObjPool = /** @class */ (function () {
        function ObjPool() {
        }
        /**
         * 预加载指定数量的对象
         * @param key 作为唯一的key
         * @param cls 一个可以new的对象
         * @param count 预加载的数量
         * @param foreach 每个item实例化后都会回调的方法
         */
        ObjPool.Reserve = function (key, cls, count, foreach) {
            if (foreach === void 0) { foreach = null; }
            for (var i = 0; i < count; i++) {
                var instance = new cls();
                foreach && foreach(instance, i);
                instance['_itemID'] = key;
                Laya.Pool.recover(key, instance);
            }
        };
        //从对象池获取一个对象
        ObjPool.Spawn = function (key) {
            return Laya.Pool.getItem(key);
        };
        //回收一个对象
        ObjPool.Despawn = function (obj) {
            obj.parent && obj.removeSelf();
            obj['_itemID'] && Laya.Pool.recover(obj['_itemID'], obj);
        };
        return ObjPool;
    }());
    es.ObjPool = ObjPool;
})(es || (es = {}));
var es;
(function (es) {
    var PointSectors;
    (function (PointSectors) {
        PointSectors[PointSectors["center"] = 0] = "center";
        PointSectors[PointSectors["top"] = 1] = "top";
        PointSectors[PointSectors["bottom"] = 2] = "bottom";
        PointSectors[PointSectors["topLeft"] = 9] = "topLeft";
        PointSectors[PointSectors["topRight"] = 5] = "topRight";
        PointSectors[PointSectors["left"] = 8] = "left";
        PointSectors[PointSectors["right"] = 4] = "right";
        PointSectors[PointSectors["bottomLeft"] = 10] = "bottomLeft";
        PointSectors[PointSectors["bottomRight"] = 6] = "bottomRight";
    })(PointSectors = es.PointSectors || (es.PointSectors = {}));
    var Collisions = /** @class */ (function () {
        function Collisions() {
        }
        Collisions.lineToLine = function (a1, a2, b1, b2) {
            var b = es.Vector2.subtract(a2, a1);
            var d = es.Vector2.subtract(b2, b1);
            var bDotDPerp = b.x * d.y - b.y * d.x;
            // 如果b*d = 0，表示这两条直线平行，因此有无穷个交点
            if (bDotDPerp == 0)
                return false;
            var c = es.Vector2.subtract(b1, a1);
            var t = (c.x * d.y - c.y * d.x) / bDotDPerp;
            if (t < 0 || t > 1)
                return false;
            var u = (c.x * b.y - c.y * b.x) / bDotDPerp;
            if (u < 0 || u > 1)
                return false;
            return true;
        };
        Collisions.lineToLineIntersection = function (a1, a2, b1, b2, intersection) {
            if (intersection === void 0) { intersection = new es.Vector2(); }
            intersection.x = 0;
            intersection.y = 0;
            var b = es.Vector2.subtract(a2, a1);
            var d = es.Vector2.subtract(b2, b1);
            var bDotDPerp = b.x * d.y - b.y * d.x;
            // 如果b*d = 0，表示这两条直线平行，因此有无穷个交点
            if (bDotDPerp == 0)
                return false;
            var c = es.Vector2.subtract(b1, a1);
            var t = (c.x * d.y - c.y * d.x) / bDotDPerp;
            if (t < 0 || t > 1)
                return false;
            var u = (c.x * b.y - c.y * b.x) / bDotDPerp;
            if (u < 0 || u > 1)
                return false;
            var temp = es.Vector2.add(a1, new es.Vector2(t * b.x, t * b.y));
            intersection.x = temp.x;
            intersection.y = temp.y;
            return true;
        };
        Collisions.closestPointOnLine = function (lineA, lineB, closestTo) {
            var v = es.Vector2.subtract(lineB, lineA);
            var w = es.Vector2.subtract(closestTo, lineA);
            var t = es.Vector2.dot(w, v) / es.Vector2.dot(v, v);
            t = es.MathHelper.clamp(t, 0, 1);
            return es.Vector2.add(lineA, new es.Vector2(v.x * t, v.y * t));
        };
        Collisions.circleToCircle = function (circleCenter1, circleRadius1, circleCenter2, circleRadius2) {
            return es.Vector2.distanceSquared(circleCenter1, circleCenter2) < (circleRadius1 + circleRadius2) * (circleRadius1 + circleRadius2);
        };
        Collisions.circleToLine = function (circleCenter, radius, lineFrom, lineTo) {
            return es.Vector2.distanceSquared(circleCenter, this.closestPointOnLine(lineFrom, lineTo, circleCenter)) < radius * radius;
        };
        Collisions.circleToPoint = function (circleCenter, radius, point) {
            return es.Vector2.distanceSquared(circleCenter, point) < radius * radius;
        };
        Collisions.rectToCircle = function (rect, cPosition, cRadius) {
            // 检查矩形是否包含圆的中心点
            if (this.rectToPoint(rect.x, rect.y, rect.width, rect.height, cPosition))
                return true;
            // 对照相关边缘检查圆圈
            var edgeFrom;
            var edgeTo;
            var sector = this.getSector(rect.x, rect.y, rect.width, rect.height, cPosition);
            if ((sector & PointSectors.top) != 0) {
                edgeFrom = new es.Vector2(rect.x, rect.y);
                edgeTo = new es.Vector2(rect.x + rect.width, rect.y);
                if (this.circleToLine(cPosition, cRadius, edgeFrom, edgeTo))
                    return true;
            }
            if ((sector & PointSectors.bottom) != 0) {
                edgeFrom = new es.Vector2(rect.x, rect.y + rect.width);
                edgeTo = new es.Vector2(rect.x + rect.width, rect.y + rect.height);
                if (this.circleToLine(cPosition, cRadius, edgeFrom, edgeTo))
                    return true;
            }
            if ((sector & PointSectors.left) != 0) {
                edgeFrom = new es.Vector2(rect.x, rect.y);
                edgeTo = new es.Vector2(rect.x, rect.y + rect.height);
                if (this.circleToLine(cPosition, cRadius, edgeFrom, edgeTo))
                    return true;
            }
            if ((sector & PointSectors.right) != 0) {
                edgeFrom = new es.Vector2(rect.x + rect.width, rect.y);
                edgeTo = new es.Vector2(rect.x + rect.width, rect.y + rect.height);
                if (this.circleToLine(cPosition, cRadius, edgeFrom, edgeTo))
                    return true;
            }
            return false;
        };
        Collisions.rectToLine = function (rect, lineFrom, lineTo) {
            var fromSector = this.getSector(rect.x, rect.y, rect.width, rect.height, lineFrom);
            var toSector = this.getSector(rect.x, rect.y, rect.width, rect.height, lineTo);
            if (fromSector == PointSectors.center || toSector == PointSectors.center) {
                return true;
            }
            else if ((fromSector & toSector) != 0) {
                return false;
            }
            else {
                var both = fromSector | toSector;
                // 线对边进行检查
                var edgeFrom = void 0;
                var edgeTo = void 0;
                if ((both & PointSectors.top) != 0) {
                    edgeFrom = new es.Vector2(rect.x, rect.y);
                    edgeTo = new es.Vector2(rect.x + rect.width, rect.y);
                    if (this.lineToLine(edgeFrom, edgeTo, lineFrom, lineTo))
                        return true;
                }
                if ((both & PointSectors.bottom) != 0) {
                    edgeFrom = new es.Vector2(rect.x, rect.y + rect.height);
                    edgeTo = new es.Vector2(rect.x + rect.width, rect.y + rect.height);
                    if (this.lineToLine(edgeFrom, edgeTo, lineFrom, lineTo))
                        return true;
                }
                if ((both & PointSectors.left) != 0) {
                    edgeFrom = new es.Vector2(rect.x, rect.y);
                    edgeTo = new es.Vector2(rect.x, rect.y + rect.height);
                    if (this.lineToLine(edgeFrom, edgeTo, lineFrom, lineTo))
                        return true;
                }
                if ((both & PointSectors.right) != 0) {
                    edgeFrom = new es.Vector2(rect.x + rect.width, rect.y);
                    edgeTo = new es.Vector2(rect.x + rect.width, rect.y + rect.height);
                    if (this.lineToLine(edgeFrom, edgeTo, lineFrom, lineTo))
                        return true;
                }
            }
            return false;
        };
        Collisions.rectToPoint = function (rX, rY, rW, rH, point) {
            return point.x >= rX && point.y >= rY && point.x < rX + rW && point.y < rY + rH;
        };
        /**
         * 位标志和帮助使用Cohen–Sutherland算法
         *
         * 位标志:
         * 1001 1000 1010
         * 0001 0000 0010
         * 0101 0100 0110
         * @param rX
         * @param rY
         * @param rW
         * @param rH
         * @param point
         */
        Collisions.getSector = function (rX, rY, rW, rH, point) {
            var sector = PointSectors.center;
            if (point.x < rX)
                sector |= PointSectors.left;
            else if (point.x >= rX + rW)
                sector |= PointSectors.right;
            if (point.y < rY)
                sector |= PointSectors.top;
            else if (point.y >= rY + rH)
                sector |= PointSectors.bottom;
            return sector;
        };
        return Collisions;
    }());
    es.Collisions = Collisions;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * 不是真正的射线(射线只有开始和方向)，作为一条线和射线。
     */
    var Ray2D = /** @class */ (function () {
        function Ray2D(position, end) {
            this.start = position;
            this.end = end;
            this.direction = es.Vector2.subtract(this.end, this.start);
        }
        return Ray2D;
    }());
    es.Ray2D = Ray2D;
})(es || (es = {}));
var es;
(function (es) {
    var SpineFactory = /** @class */ (function () {
        function SpineFactory() {
        }
        /**
         * 初始化
         * @param ver Spine版本号
         */
        SpineFactory.init = function (ver) {
            SpineFactory.version = ver;
        };
        /**
         * 提前预加载
         * @param url .skel文件url
         * @param cb 加载完成回调
         */
        SpineFactory.load = function (url, cb) {
            SpineFactory.loads([url], cb);
        };
        /**
         * 提前预加载
         * @param urls  Array<.skel>
         * @param cb 加载完成回调
         */
        SpineFactory.loads = function (urls, cb) {
            for (var i = 0; i < urls.length; i++) {
                var url = urls[i];
                if (!SpineFactory.cachePool[url]) {
                    var st = SpineFactory.cachePool[url] = new Laya.SpineTemplet(SpineFactory.version);
                    (i == urls.length - 1) && SpineFactory.bindLoadedCallback(st, cb);
                    st.on(Laya.Event.COMPLETE, SpineFactory, SpineFactory.loadComplete);
                    st.on(Laya.Event.ERROR, SpineFactory, SpineFactory.loadFail);
                    st.loadAni(url);
                }
                else if (!SpineFactory.cachePool[url]['__loaded']) {
                    SpineFactory.bindLoadedCallback(SpineFactory.cachePool[url], cb);
                }
                else {
                    cb && cb.runWith(SpineFactory.cachePool[url]);
                }
            }
        };
        /** 添加 加载结束回调 */
        SpineFactory.bindLoadedCallback = function (st, cb) {
            var cbs = SpineFactory.loadedCallback.get(st);
            if (!cbs) {
                cbs = [];
            }
            cbs.push(cb);
            SpineFactory.loadedCallback.set(st, cbs);
        };
        /** 加载完成 */
        SpineFactory.loadComplete = function (obj) {
            obj['__loaded'] = true;
            var cbs = SpineFactory.loadedCallback.get(obj);
            cbs && [].forEach.call(cbs, function (cb) {
                cb && cb.runWith(obj);
            });
        };
        /** 加载失败 */
        SpineFactory.loadFail = function (e) {
            console.error(e);
        };
        /**
         * 生成新的龙骨
         * @param in_url 龙骨url
         * @param out_res 加载完成后的回调
         */
        SpineFactory.spawn = function (in_url, out_res) {
            if (SpineFactory.cachePool[in_url] && SpineFactory.cachePool[in_url]['__loaded']) {
                out_res(SpineFactory.cachePool[in_url].buildArmature());
            }
            else {
                SpineFactory.load(in_url, Laya.Handler.create(this, function (st) {
                    out_res(st.buildArmature());
                }));
            }
        };
        /**
         * 添加一个动画播放停止的事件监听
         * @param spine
         * @param handler
         * @param once
         */
        SpineFactory.AddCompleteHandle = function (spine, caller, func, once) {
            if (once === void 0) { once = true; }
            if (once) {
                spine.once(Laya.Event.STOPPED, caller, func);
            }
            else {
                spine.on(Laya.Event.STOPPED, caller, func);
            }
        };
        /**
         * 移除Spine动画 停止事件监听
         *
         * @param spine 指定的Laya.SpineSkeleton实例
         * @param caller 如果 caller 和 func 都存在 则指定删除，如果仅有caller，则移除spine上所有关于caller的监听
         * @param func 仅配合caller使用 指定删除某对象某一方法
         */
        SpineFactory.RemoveCompleteHandle = function (spine, caller, func) {
            if (caller && func) {
                spine.off(Laya.Event.STOPPED, caller, func);
            }
            else if (caller) {
                spine.offAllCaller(caller);
            }
            else {
                spine.offAll(Laya.Event.STOPPED);
            }
        };
        /** 缓存池 */
        SpineFactory.cachePool = {};
        /** 加载完成事件 */
        SpineFactory.loadedCallback = new Map();
        return SpineFactory;
    }());
    es.SpineFactory = SpineFactory;
})(es || (es = {}));
var es;
(function (es) {
    var SpinePool = /** @class */ (function () {
        function SpinePool() {
        }
        SpinePool.PushBack = function (spineInst, sign) {
            if (!spineInst) {
                es.Debug.error("You try push nullptr to SpinePool. sign: " + sign);
                return;
            }
            if (undefined == spineInst['POOL_KEY'] && !sign) {
                es.Debug.error("Pool Key undefined");
                es.Debug.debug && console.log(spineInst);
                return;
            }
            var pool = SpinePool._pool;
            var idx = pool.indexOf(spineInst);
            (idx == -1) && pool.push(spineInst);
            sign = sign || spineInst['POOL_KEY'];
            sign && SpinePool.Clean(spineInst);
            sign && SpinePool.Sign(sign, spineInst);
            sign && Laya.Pool.recover(sign, spineInst);
        };
        SpinePool.Sign = function (poolSign, spineInst) {
            spineInst['POOL_KEY'] = poolSign;
        };
        SpinePool.Clean = function (spineInst) {
            spineInst.parent && spineInst.removeSelf();
            spineInst.offAll(Laya.Event.STOPPED);
            spineInst.stop();
        };
        SpinePool.Malloc = function (sign) {
            if (!sign || sign.length < 1) {
                es.Debug.error("error sign: " + sign);
                return null;
            }
            return Laya.Pool.getItem(sign);
        };
        SpinePool.Recycle = function (spineInst) {
            if (!spineInst || !spineInst['POOL_KEY']) {
                es.Debug.error("Recycle: spineInst is null or not pool object");
                return;
            }
            SpinePool.Clean(spineInst);
            Laya.Pool.recover(spineInst['POOL_KEY'], spineInst);
        };
        SpinePool.TrackAllSpineRecycle2LayaPool = function () {
            var pool = SpinePool._pool;
            for (var i = 0; i < pool.length; i++) {
                if (!pool[i] || pool[i].destroyed) {
                    pool.splice(i--, 1);
                    continue;
                }
                SpinePool.Clean(pool[i]);
                Laya.Pool.recover(pool[i]['POOL_KEY'], pool[i]);
            }
        };
        SpinePool._pool = [];
        return SpinePool;
    }());
    es.SpinePool = SpinePool;
})(es || (es = {}));
var es;
(function (es) {
    /**
     *  第三方消息派发工具类
     */
    var Third = /** @class */ (function () {
        function Third() {
        }
        /**
         * 监听事件
         * @param event 事件名称
         * @param caller 作用域
         * @param handl 回调方法
         */
        Third.on = function (event, caller, handl) {
            var e_1, _a;
            if (undefined == Third._handls[event]) {
                Third._handls[event] = [{ caller: caller, handl: handl }];
            }
            else {
                var evlist = Third._handls[event];
                try {
                    for (var evlist_1 = __values(evlist), evlist_1_1 = evlist_1.next(); !evlist_1_1.done; evlist_1_1 = evlist_1.next()) {
                        var ev = evlist_1_1.value;
                        if (ev.caller == caller && ev.handl == handl) {
                            console.log("############## Repeat listening", event, caller, handl);
                            return;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (evlist_1_1 && !evlist_1_1.done && (_a = evlist_1.return)) _a.call(evlist_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                Third._handls[event].push({ caller: caller, handl: handl });
            }
        };
        /**
         * 注册节点的特定事件类型回调，回调会在第一时间被触发后删除自身
         * @param event 事件名称
         * @param caller 作用域
         * @param handl 回调方法
         */
        Third.once = function (event, caller, handl) {
            if (undefined == Third._handls[event]) {
                Third._handls[event] = [{ caller: caller, handl: handl, once: true }];
            }
            else {
                Third._handls[event].push({ caller: caller, handl: handl, once: true });
            }
        };
        /**
         * 移除监听
         * @param event 事件名称
         * @param caller 作用域 如果忽略 则移除所有该事件的监听 off("show");//所有show事件相关监听都会被移除
         * @param handl 回调方法 如果忽略 则移除该事件列表里 所有caller相关的方法  off("show",this);// this作用域下所有对show的监听都会被移除
         */
        Third.off = function (event, caller, handl) {
            if (caller === void 0) { caller = undefined; }
            if (handl === void 0) { handl = undefined; }
            //获取事件队列
            var list = Third._handls[event];
            //遍历所有事件
            if (list && list instanceof Array) {
                //移除所有当前事件的监听
                if (undefined == caller) {
                    Third._handls[event] = [];
                    return;
                }
                var e = null;
                for (var i = 0; i < list.length; i++) {
                    e = list[i];
                    //移除对应事件
                    if (e.caller == caller && (undefined == handl || handl == e.handl)) {
                        //交换指针 移除尾部
                        list.splice(i--, 1);
                    }
                }
            }
        };
        /**
         * 移除监听者的所有事件监听
         * @param caller 监听者 如果为null 则进行clear   offall(); 事件池归零
         */
        Third.offall = function (caller) {
            if (caller === void 0) { caller = undefined; }
            if (!caller) {
                Third._handls = {};
            }
            else {
                //遍历所有事件链表
                for (var k in Third._handls) {
                    var list = Third._handls[k];
                    if (list && list instanceof Array) {
                        for (var i = 0; i < list.length; i++) {
                            //移除对应事件
                            if (list[i].caller == caller) {
                                list.splice(i--, 1);
                            }
                        }
                    }
                }
            }
        };
        /**
         * 派发事件
         * @param event 事件
         * @param params 可变参列表  do("show",true);   do("show",true,data);  do("show",data,true,"hello world",1234567890);
         */
        Third.do = function (event) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            var _a;
            var list = Third._handls[event];
            if (list && list instanceof Array) {
                var t = null;
                for (var i = 0; i < list.length; i++) {
                    //触发回调
                    t = list[i];
                    (_a = t.handl).call.apply(_a, __spread([t.caller], params));
                    //仅触发一次 则触发后移除
                    if (t.once) {
                        list.splice(i--, 1);
                    }
                }
            }
        };
        Third._handls = {};
        return Third;
    }());
    es.Third = Third;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * Class ThirdSupport
     *
     * .You can inherit it to gain the ability to dispatch events
     *
     * @anchor ChenJC | Geek7
     * @date 20220615
     */
    var ThirdSupport = /** @class */ (function () {
        function ThirdSupport() {
            this.persistentHandler = new Map();
            this.onceHandler = new Map();
        }
        ThirdSupport.prototype.on = function (eventType, caller, fn) {
            if (this.persistentHandler.has(eventType)) {
                var handler = this.persistentHandler.get(eventType).find(function (f) { return f.equal2(caller, fn); });
                if (null != handler) {
                    es.Debug.log("$$$$$$$$$$ handler already listen: " + handler.toString(), "#ffff00");
                }
            }
            else {
                this.persistentHandler.set(eventType, [es.Functor.New(fn, caller)]);
            }
        };
        ThirdSupport.prototype.once = function (eventType, caller, fn) {
            if (this.onceHandler.has(eventType)) {
                var handler = this.onceHandler.get(eventType).find(function (f) { return f.equal2(caller, fn); });
                if (null != handler) {
                    es.Debug.log("$$$$$$$$$$ handler already listen: " + handler.toString(), "#ffff00");
                }
            }
            else {
                this.onceHandler.set(eventType, [es.Functor.New(fn, caller)]);
            }
        };
        ThirdSupport.prototype.event = function (eventType) {
            var e_2, _a, e_3, _b;
            if (this.persistentHandler.has(eventType)) {
                var handerArr = this.persistentHandler.get(eventType);
                try {
                    for (var handerArr_1 = __values(handerArr), handerArr_1_1 = handerArr_1.next(); !handerArr_1_1.done; handerArr_1_1 = handerArr_1.next()) {
                        var handler = handerArr_1_1.value;
                        handler.invoke();
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (handerArr_1_1 && !handerArr_1_1.done && (_a = handerArr_1.return)) _a.call(handerArr_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            if (this.onceHandler.has(eventType)) {
                var handerArr = this.onceHandler.get(eventType);
                try {
                    for (var handerArr_2 = __values(handerArr), handerArr_2_1 = handerArr_2.next(); !handerArr_2_1.done; handerArr_2_1 = handerArr_2.next()) {
                        var handler = handerArr_2_1.value;
                        handler.invoke();
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (handerArr_2_1 && !handerArr_2_1.done && (_b = handerArr_2.return)) _b.call(handerArr_2);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
            }
        };
        ThirdSupport.prototype.eventArgs = function (eventType) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var e_4, _a, e_5, _b;
            if (this.persistentHandler.has(eventType)) {
                var handerArr = this.persistentHandler.get(eventType);
                try {
                    for (var handerArr_3 = __values(handerArr), handerArr_3_1 = handerArr_3.next(); !handerArr_3_1.done; handerArr_3_1 = handerArr_3.next()) {
                        var handler = handerArr_3_1.value;
                        handler.invokeArgs.apply(handler, __spread(args));
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (handerArr_3_1 && !handerArr_3_1.done && (_a = handerArr_3.return)) _a.call(handerArr_3);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
            }
            if (this.onceHandler.has(eventType)) {
                var handerArr = this.onceHandler.get(eventType);
                try {
                    for (var handerArr_4 = __values(handerArr), handerArr_4_1 = handerArr_4.next(); !handerArr_4_1.done; handerArr_4_1 = handerArr_4.next()) {
                        var handler = handerArr_4_1.value;
                        handler.invokeArgs.apply(handler, __spread(args));
                    }
                }
                catch (e_5_1) { e_5 = { error: e_5_1 }; }
                finally {
                    try {
                        if (handerArr_4_1 && !handerArr_4_1.done && (_b = handerArr_4.return)) _b.call(handerArr_4);
                    }
                    finally { if (e_5) throw e_5.error; }
                }
            }
        };
        /**
         *
         * @param eventType if this value is null, then delete all handelr for caller
         * @param caller if this value is null and eventType is null, then clear all handler
         * @param fn if this value is null, then clear caller all method.
         */
        ThirdSupport.prototype.off = function (eventType, caller, fn) {
            if (eventType === void 0) { eventType = null; }
            if (caller === void 0) { caller = null; }
            if (fn === void 0) { fn = null; }
            if (eventType == null) {
                if (!caller) {
                    this.persistentHandler.clear();
                    this.onceHandler.clear();
                }
                else {
                    this.persistentHandler.forEach(function (arr) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].getCaller() == caller && (arr[i].getMethod() == fn || !fn)) {
                                arr.splice(i--, 1);
                            }
                        }
                    });
                    this.onceHandler.forEach(function (arr) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].getCaller() == caller && (arr[i].getMethod() == fn || !fn)) {
                                arr.splice(i--, 1);
                            }
                        }
                    });
                }
            }
            else {
                if (this.persistentHandler.has(eventType)) {
                    var arr = this.persistentHandler.get(eventType);
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].getCaller() == caller && (!fn || fn == arr[i].getMethod())) {
                            arr.splice(i--, 1);
                        }
                    }
                }
                if (this.onceHandler.has(eventType)) {
                    var arr = this.onceHandler.get(eventType);
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].getCaller() == caller && (!fn || fn == arr[i].getMethod())) {
                            arr.splice(i--, 1);
                        }
                    }
                }
            }
        };
        return ThirdSupport;
    }());
    es.ThirdSupport = ThirdSupport;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * 所有的 TimelineSprite 对象 统一调度管理
     * 全局只产生一个定时器
     */
    var TimelineFactory = /** @class */ (function () {
        /**
         * 不允许自行创建
         */
        function TimelineFactory() {
            this.elements = [];
        }
        ;
        /**
         * 不允许业务逻辑自行创建
         */
        TimelineFactory.genSingleton = function () {
            TimelineFactory.instance = new TimelineFactory();
            Laya.timer.frameLoop(1, TimelineFactory.instance, TimelineFactory.instance.update);
        };
        /**
         * 更新
         */
        TimelineFactory.prototype.update = function () {
            if (this.elements.length > 0) {
                var element = void 0, delta = Laya.timer.delta;
                for (var i = 0; i < this.elements.length; i++) {
                    element = this.elements[i];
                    element['tm'] += delta;
                    if (element['tm'] >= element['interval']) {
                        element['tm'] -= element['interval'];
                        element['updateFrame']();
                    }
                }
            }
        };
        TimelineFactory.prototype.appendTimeline = function (timeline) {
            var index = this.elements.indexOf(timeline);
            if (index == -1) {
                this.elements.push(timeline);
            }
        };
        TimelineFactory.prototype.removeTimeline = function (timelineOrIndex) {
            if (typeof timelineOrIndex == "number") {
                if (timelineOrIndex >= 0 && timelineOrIndex < this.elements.length) {
                    this.elements.splice(timelineOrIndex, 1);
                }
            }
            else {
                var index = this.elements.indexOf(timelineOrIndex);
                if (index != -1) {
                    this.elements.splice(index, 1);
                }
            }
        };
        /**
         * 主动释放
         * 一般情况下 整个游戏生命周期内 都不需要调用 你可以忽视它
         */
        TimelineFactory.prototype.release = function () {
            this.elements = [];
            Laya.timer.clear(TimelineFactory.instance, TimelineFactory.instance.update);
        };
        return TimelineFactory;
    }());
    es.TimelineFactory = TimelineFactory;
})(es || (es = {}));
var es;
(function (es) {
    /**
     * 使用案例
     * https://blog.csdn.net/qq_39162566/article/details/125105592
     */
    var TimelineSprite = /** @class */ (function (_super) {
        __extends(TimelineSprite, _super);
        /**
         * 构造方法
         * @param frames 动画帧数组
         * @param frameRate 帧率 例如: frameRate = 30 那每帧的间隔就是33.33ms ( 1000 / 30 )
         */
        function TimelineSprite(frames, frameRate) {
            if (frameRate === void 0) { frameRate = 30; }
            var _this = _super.call(this) || this;
            _this.loop = false;
            _this.index = 0;
            _this.frames = [];
            _this.frameEvent = {};
            _this.frames = frames;
            _this.interval = Math.floor(1000.0 * 1000 / frameRate) / 1000;
            _this.tm = 0;
            return _this;
        }
        /**
         * 设置新的动画组
         * @param frames 动画帧数组
         * @param frameRate 帧率 可以不填 采用上一个动画的方案 默认30帧每秒
         */
        TimelineSprite.prototype.setNewAnimationFrames = function (frames, frameRate) {
            if (frameRate)
                this.interval = Math.floor(1000.0 * 1000 / frameRate) / 1000;
            this.frames = frames;
        };
        /**
         * 播放
         * @param loop 是否循环播放  默认false
         * @param start 开始播放的位置 百分百 0~1  循环中也是以这个为主
         * @param end 结束播放的位置 百分比 0～1
         */
        TimelineSprite.prototype.play = function (loop, start, end) {
            if (loop === void 0) { loop = false; }
            if (start === void 0) { start = 0; }
            if (end === void 0) { end = 1.0; }
            this.tm = 0;
            this.loop = loop;
            this.play_begin_pos = Math.floor(start * this.frames.length);
            this.play_end_pos = Math.floor(end * this.frames.length);
            this.index = this.play_begin_pos;
            this.last_frame = Laya.loader.getRes(this.frames[this.index]);
            this.add2aniPool();
        };
        /**
         * 添加一个事件
         * @param pos 位置 百分比 0～1
         * @param caller
         * @param fun
         * @param once 是否执行一次后自动移除
         */
        TimelineSprite.prototype.addEvent = function (pos, caller, fun, once) {
            if (once === void 0) { once = false; }
            var frameIndex = Math.floor(pos * this.frames.length);
            var handlers = this.frameEvent[frameIndex];
            if (handlers && handlers.find(function (handler) {
                return handler.caller == caller && handler.method == fun;
            }) != null) {
                return;
            }
            handlers = this.frameEvent[frameIndex] || [];
            handlers.push(Laya.Handler.create(caller, fun, null, once));
            this.frameEvent[frameIndex] = handlers;
        };
        /**
         * 停止播放
         * 会停留在当前帧
         * 你可以使用 visible = false; 来隐藏 动画展示
         */
        TimelineSprite.prototype.stop = function () {
            this.removeFromPool();
        };
        /**
         *
         * 更新帧
         */
        TimelineSprite.prototype.updateFrame = function () {
            this.onframeEvent();
            ++this.index;
            if (this.index >= this.play_end_pos) {
                this.event(TimelineSprite.EVENT_ENDFRAME);
                if (!this.loop) {
                    this.removeFromPool();
                    this.event(TimelineSprite.EVENT_ENDED);
                    return;
                }
                else {
                    this.index = this.play_begin_pos;
                }
            }
            this.last_frame = Laya.loader.getRes(this.frames[this.index]);
            this.draw();
        };
        TimelineSprite.prototype.onframeEvent = function () {
            var events = this.frameEvent[this.index];
            if (events) {
                for (var i = 0; i < events.length; i++) {
                    events[i].run();
                    if (events[i].once) {
                        events.splice(i--, 1);
                    }
                }
            }
        };
        TimelineSprite.prototype.draw = function () {
            this.graphics.clear();
            this.graphics.drawImage(this.last_frame);
        };
        TimelineSprite.prototype.add2aniPool = function () {
            !es.TimelineFactory['instance'] && es.TimelineFactory['genSingleton']();
            es.TimelineFactory['instance'].appendTimeline(this);
        };
        TimelineSprite.prototype.removeFromPool = function () {
            es.TimelineFactory['instance'] && es.TimelineFactory['instance'].removeTimeline(this);
        };
        TimelineSprite.prototype.clone = function () {
            var obj = new TimelineSprite(this.frames);
            for (var k in this) {
                if (typeof k == "number" || typeof k == "boolean") {
                    obj[k] = this[k];
                }
            }
            return obj;
        };
        /**
         * 最后一帧派发
         */
        TimelineSprite.EVENT_ENDFRAME = "EVENT_ENDFRAME";
        /**
         * 非循环播放结束时触发
         */
        TimelineSprite.EVENT_ENDED = "EVENT_ENDED";
        return TimelineSprite;
    }(Laya.Sprite));
    es.TimelineSprite = TimelineSprite;
})(es || (es = {}));
var es;
(function (es) {
    /** 新建一个tween动作 */
    function tween(target) {
        var act = new tween_action();
        if (!target) {
            es.Debug.error("You trying to create a es.tween for an nullptr");
            return act;
        }
        act['init'](target);
        return act;
    }
    es.tween = tween;
    /**
     * 获取未完成的tween动作数量
     * @param target 执行tween的对象
     * @returns
     */
    function getUndoneActionCount(target) {
        if (target && target['gk7_tweenCount'])
            return target['gk7_tweenCount'].invoke();
        return 0;
    }
    es.getUndoneActionCount = getUndoneActionCount;
    /**
     * 清除action
     * @param target 执行tween的对象
     * @param quickComplete 是否立即完成剩余动作,会顺序执行每个阶段最后一次update,包括回调方法
     */
    function killTween(target, quickComplete) {
        if (quickComplete === void 0) { quickComplete = false; }
        target && target['gk7_kill'] && target['gk7_kill'].invoke(quickComplete);
    }
    es.killTween = killTween;
    /** tween工厂 */
    var tween_core = /** @class */ (function () {
        function tween_core() {
            this._actions = [];
            this._timerID = -1;
        }
        //大时钟 调度
        tween_core.prototype.update = function () {
            var acts = this._actions;
            var act = null;
            var delta = Laya.timer.delta * 0.001;
            for (var i = 0; i < acts.length; i++) {
                act = acts[i];
                if (act.isDone()) {
                    --act._times;
                    if (act._times === 0) {
                        acts.splice(i, 1);
                        --i;
                        continue;
                    }
                    act.resume();
                }
                act.update(delta);
            }
        };
        //@friend tween_action 隐藏 不允许被外部调用 仅供 tween_action 类调度
        tween_core.prototype.add = function (act) {
            this._actions.push(act);
        };
        //@friend tween_action 隐藏 不允许被外部调用 仅供 tween_action 类调度ß
        tween_core.prototype.sub = function (act) {
            var index = this._actions.indexOf(act);
            index != -1 && this._actions.splice(index, 1);
        };
        /** 停止tween所有tick  PS: 此操作会停下所有的动作 */
        tween_core.prototype.stop = function () {
            if (this._timerID == -1)
                return;
            this._timerID = -1;
            Laya.timer.clear(this, this.update);
            // window.clearInterval(this._timerID);
        };
        /** 开启/恢复tween所有tick */
        tween_core.prototype.resume = function () {
            if (-1 == this._timerID) {
                Laya.timer.frameLoop(1, this, this.update);
                this._timerID = 1;
                // this._timerID = window.setInterval(this.update.bind(this), 1000 / 60.0);
            }
        };
        /** 清理所有tween */
        tween_core.prototype.clear = function () {
            this._actions = [];
        };
        Object.defineProperty(tween_core, "inst", {
            get: function () {
                if (!tween_core._inst) {
                    tween_core._inst = new tween_core();
                    tween_core._inst.resume();
                }
                return tween_core._inst;
            },
            enumerable: true,
            configurable: true
        });
        tween_core._inst = null;
        return tween_core;
    }());
    es.tween_core = tween_core;
    /** 维护tween链 */
    var tween_action = /** @class */ (function () {
        function tween_action() {
            this._target = null;
            this._index = 0;
            this._commands = [];
            this._times = 1;
        }
        tween_action.prototype.init = function (target) {
            this._target = target;
            this._target['gk7_kill'] = es.Functor.New(this.clear, this);
            this._target['gk7_tweenCount'] = es.Functor.New(this.getUndoneActionCount, this);
        };
        /**
         * 缓动
         * @param dur 持续时间
         * @param props 目标属性
         * @param easeFn ease处理方法 例如: Laya.Ease.backInOut
         */
        tween_action.prototype.to = function (dur, props, easeFn) {
            if (easeFn === void 0) { easeFn = null; }
            var t = new command(this._target);
            t._tm = 0;
            t._dur = dur;
            t._dest = props;
            t._easeHandler = easeFn || Laya.Ease.linearNone;
            var b = this._commands.length > 0 ? this._commands[this._commands.length - 1]._dest : this._target;
            for (var k in props) {
                t._src[k] = k in b ? b[k] : this._target[k];
            }
            this._commands.push(t);
            return this;
        };
        /**
         * 贝塞尔运动
         * es.tween(this.sprite).bezier(1.0,new Laya.Point(0,0),
         *      new Laya.Point(100,100),
         *      new Laya.Point(300,0)).call( this.callbackFunction ).start();
         *
         */
        tween_action.prototype.bezier = function (dur, startPoint, peakPoint, endPoint) {
            var t = new bezierCommand(this._target, dur, startPoint, peakPoint, endPoint);
            this.fullPreStateDest(t);
            this._commands.push(t);
            return this;
        };
        /**
         * 加入一个回调
         * @param fn 回调
         */
        tween_action.prototype.call = function (fn) {
            var t = new callbackCommand(fn);
            this.fullPreStateDest(t);
            this._commands.push(t);
            return this;
        };
        /**
         * 在动作间添加一个间隔
         * @param time 间隔秒
         */
        tween_action.prototype.delay = function (time) {
            var t = new delayCommand(this._target);
            this.fullPreStateDest(t);
            t._tm = 0;
            t._dur = time;
            this._commands.push(t);
            return this;
        };
        /**
         * 循环 当 times < 0 无限循环  为0时 不执行动作  为1时执行一次
         * @param times 次数 默认1
         */
        tween_action.prototype.loop = function (times) {
            if (times === void 0) { times = 1; }
            this._times = times;
            return this;
        };
        /**
         * 开启缓动
         */
        tween_action.prototype.start = function () {
            tween_core.inst['add'](this);
        };
        /** 清除动作 是否立即完成所有动作 */
        tween_action.prototype.clear = function (immediately) {
            var e_6, _a;
            //从更新池中移除
            tween_core.inst['sub'](this);
            //如果选择了立即完成
            if (immediately) {
                try {
                    for (var _b = __values(this._commands), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var command_1 = _c.value;
                        command_1.immediatelyComplete();
                    }
                }
                catch (e_6_1) { e_6 = { error: e_6_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_6) throw e_6.error; }
                }
            }
            this._commands = [];
        };
        /** 获取未完成的动作数量 */
        tween_action.prototype.getUndoneActionCount = function () {
            return this._commands.length - this._index;
        };
        //将最后一个动作的结束值作为当前动作的结束状态
        tween_action.prototype.fullPreStateDest = function (curren) {
            curren._dest = this._commands.length > 0 ? this._commands[this._commands.length - 1]._dest : this._target;
        };
        // 恢复所有动作
        tween_action.prototype.resume = function () {
            var e_7, _a;
            this._index = 0;
            try {
                for (var _b = __values(this._commands), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var com = _c.value;
                    com._tm = 0;
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_7) throw e_7.error; }
            }
        };
        // 每帧更新
        tween_action.prototype.update = function (dt) {
            var com = this._commands[this._index];
            if (!com.isDone()) {
                com.update(dt);
            }
            else {
                ++this._index;
            }
        };
        // 完成
        tween_action.prototype.isDone = function () {
            return this._index >= this._commands.length;
        };
        return tween_action;
    }());
    es.tween_action = tween_action;
    /** 基础command, 主要对属性进行线性插值 */
    var command = /** @class */ (function () {
        function command(target) {
            this._tm = 0;
            this._dur = 0;
            this._src = {};
            this._dest = {};
            this._target = null;
            this._easeHandler = null;
            this._target = target;
        }
        command.prototype.isDone = function () {
            return this._dur === 0 || this._tm / this._dur >= 1.0;
        };
        command.prototype.update = function (dt) {
            this._tm += dt;
            this._tm = this._tm > this._dur ? this._dur : this._tm;
            var r = this._easeHandler(this._tm, 0, 1, this._dur);
            for (var k in this._dest) {
                this._target[k] = this._src[k] + (this._dest[k] - this._src[k]) * r;
            }
        };
        /** 立即完成当前命令 */
        command.prototype.immediatelyComplete = function () {
            if (this.isDone())
                return;
            this._tm = this._dur;
            this.update(1 / 60);
        };
        return command;
    }());
    es.command = command;
    /** 延时 */
    var delayCommand = /** @class */ (function (_super) {
        __extends(delayCommand, _super);
        function delayCommand() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        delayCommand.prototype.isDone = function () {
            return this._dur === 0 || this._tm / this._dur >= 1.0;
        };
        delayCommand.prototype.update = function (dt) {
            this._tm += dt;
            this._tm = this._tm > this._dur ? this._dur : this._tm;
        };
        delayCommand.prototype.immediatelyComplete = function () {
            this._tm = this._dur;
        };
        return delayCommand;
    }(command));
    /** 回调command */
    var callbackCommand = /** @class */ (function (_super) {
        __extends(callbackCommand, _super);
        function callbackCommand(fn) {
            var _this = _super.call(this, null) || this;
            _this._fn = null;
            _this._tm = 0;
            _this._dur = 1 / 60.0;
            _this._fn = fn;
            return _this;
        }
        callbackCommand.prototype.isDone = function () {
            this._fn && this._fn();
            this._fn = null; //保证只执行一次
            return true;
        };
        return callbackCommand;
    }(command));
    /** 贝塞尔command */
    var bezierCommand = /** @class */ (function (_super) {
        __extends(bezierCommand, _super);
        function bezierCommand(target, dur, startPoint, peakPoint, endPoint) {
            var _this = _super.call(this, target) || this;
            _this._dur = dur;
            _this.posArr = [
                startPoint,
                peakPoint,
                endPoint
            ];
            return _this;
        }
        bezierCommand.prototype.update = function (dt) {
            this._tm += dt;
            var value = Math.min(this._tm / this._dur, 1.0);
            var posArr = this.posArr;
            this._target.x = (1 - value) * (1 - value) * posArr[0].x + 2 * value * (1 - value) * posArr[1].x + value * value * posArr[2].x;
            this._target.y = (1 - value) * (1 - value) * posArr[0].y + 2 * value * (1 - value) * posArr[1].y + value * value * posArr[2].y;
        };
        return bezierCommand;
    }(command));
})(es || (es = {}));
