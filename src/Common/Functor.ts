module es {

    export class Functor {

        protected fn: Function;
        protected caller: any;
        protected constructor(fn: Function, caller: any) {
            this.caller = caller;
            this.fn = fn;
        }


        /**@static 创建一个仿函数 */
        public static New(fn: Function, caller: any) {
            return new Functor(fn, caller);
        }

        /**@static 判断两个仿函数是否相等 */
        public static Equal(me: Functor, he: Functor) {
            return me.caller == he.caller && me.fn == he.fn;
        }

        /** 通过调用者 和 方法来判断是否相等 */
        public equal2(caller:any,fn:Function){
            return this.caller == caller && this.fn == fn;
        }

        /** 判断两个仿函数是否相等 */
        public equal(he: Functor) {
            return Functor.Equal(this, he);
        }
        
        /** 获取调用者 */
        public getCaller(){
            return this.caller;
        }
        /** 获取方法 */
        public getMethod(){
            return this.fn;
        }

        /** 调用 */
        public invoke() {
            return this.fn.call(this.caller);
        }
        /** 带参数调用 */
        public invokeArgs(...args){
            return this.fn.call(this.caller,...args);
        }
        /** 重写toString */
        public toString() {
            return `${this.caller.toString()}.${this.fn.toString()}(...args)`;
        }
    }
}