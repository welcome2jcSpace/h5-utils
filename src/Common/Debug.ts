module es {

    export class Debug {
        /** 是否在debug模式 */
        public static debug = true;
        /**
         * 打印
         * @param content 文本内容
         * @param fontColor 字体颜色
         * @param bgColor 背景颜色
         */
        public static log(content, fontColor = "#eeee33", bgColor = "#000000") {
            Debug.debug && Debug.eventLog(content) && console.log(`%c${content}`, `color:${fontColor};background:${bgColor};padding:3px 6px;`);
        }

        /**
         * 打印报错
         * @param content 文本内容
         */
        public static error(content) {
            this.log("############" + content + "############", "#ff0000", "#000000");
        }

        /**
         * DEBUG模式下 打印日志的内容派发
         */
        public static readonly EVENT_DEBUG_LOG: string = "EVENT_DEBUG_LOG";
        protected static eventLog(content) {
            Third.do(Debug.EVENT_DEBUG_LOG, content);
            return true;
        }

        /**
         * 获取当前平台信息
         */
        public static getPlatformInfo() {
            return {
                /** 当前浏览器 user-Agent 字符串*/
                userAgent: navigator.userAgent.toLowerCase(),
                /** 当前运行在安卓平台上 */
                onAndroid: Boolean(navigator.userAgent.match(/android/ig)),
                /** 当前运行在苹果平台上 */
                onIOS: Boolean(navigator.userAgent.match(/iphone|ipod/ig)),
                /** 当前运行在Ipad平台上 */
                onIpad: Boolean(navigator.userAgent.match(/ipad/ig)),
                /** 当前运行在微信平台上 */
                onWechat: Boolean(navigator.userAgent.match(/MicroMessenger/ig))
            };
        }



        private static _gfx: Laya.Graphics
        private static get gfx(): Laya.Graphics {
            if (!Debug._gfx) {
                // //找到活动中的场景
                // for (let view of Laya.Scene.unDestroyedScenes) {
                //     if (view._parent) {
                //         let sprite = new Laya.Sprite();
                //         sprite.name = "DEBUG_GFX_SPRITE";
                //         view.addChild(sprite);
                //         Debug._gfx = sprite.graphics;
                //         break;
                //     }
                // }
                let sprite = new Laya.Sprite();
                sprite.name = "DEBUG_GFX_SPRITE";
                Laya.Scene.root.parent.addChild(sprite);
                Debug._gfx = sprite.graphics;
                //调一个定时器结束自动清理
                Laya.lateTimer.loop(1, this, () => {
                    Debug._gfx.clear(true);
                });
            }
            return Debug._gfx;
        }
        /**
         * 绘制debug线框
         * @param star 开始 global pos 
         * @param end 结束
         * @param color 颜色
         */
        public static DrawLine(star: { x: number, y: number }, end: { x: number, y: number }, color: string = "#ff0000") {
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.drawLine(star.x, star.y, end.x, end.y, color, 2);
            }
        }

        /**
         * 绘制边框
         * @param posOrRect  global pos 
         * @param sizeOrColor 
         * @param color 
         */
        public static DrawRectangles(posOrRect: { x: number, y: number } | { x: number, y: number, width: number, height: number }, sizeOrColor: { x: number, y: number } | string, color?: string) {
            if (Debug.debug && Debug.gfx) {
                if (!posOrRect['width']) {
                    posOrRect['width'] = sizeOrColor['x'];
                    posOrRect['height'] = sizeOrColor['y'];
                    color = color || "#ff0000";
                }
                if (typeof (sizeOrColor) == "string") {
                    color = sizeOrColor || "#ff0000";
                }
                Debug.gfx.drawRect(posOrRect.x, posOrRect.y, posOrRect['width'], posOrRect['height'], undefined, color, 2);
            }
        }

        /**
         * 绘制圆框
         * @param pos 
         * @param r 
         * @param color 
         */
        public static DrawCircle(pos: { x: number, y: number }, r: number, color: string = "ff0000") {
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.drawCircle(pos.x, pos.y, r, undefined, color, 2);
            }
        }

        /**
         * 绘制扇形
         * @param pos 位置 
         * @param r 半径
         * @param startAngle 开始角度 
         * @param endAngle end角度
         * @param color 颜色
         */
        public static DrawPie(pos: { x: number, y: number }, r: number, startAngle: number, endAngle: number, color: string = "ff0000") {
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.drawPie(pos.x, pos.y, r, startAngle, endAngle, undefined, color, 2);
            }
        }

        /**
         * 绘制文字
         * @param text 内容
         * @param pos 位置
         * @param color 字体颜色
         * @param fontSize 字体像素大小
         */
        public static GUILabel(text: string, pos: { x: number, y: number }, color: string = "ffffff", fontSize = 20) {
            if (Debug.debug && Debug.gfx) {
                Debug.gfx.fillText(text, pos.x, pos.y, `${fontSize}px Arial`, color, "left");
            }
        }

    }

}