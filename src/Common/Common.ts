module es {


    export class Common {

        /**
         * 浅拷贝 
         * @param src 拷贝对象
         * @param dest 覆盖对象
         */
        public static Clone(src, dest): void {
            for (let k in src) {
                dest[k] = src[k];
            }
        }
    }
}