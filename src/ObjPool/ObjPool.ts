module es {
    export class ObjPool {
        /**
         * 预加载指定数量的对象
         * @param key 作为唯一的key 
         * @param cls 一个可以new的对象
         * @param count 预加载的数量
         * @param foreach 每个item实例化后都会回调的方法
         */
        public static Reserve(key: string, cls: new () => any, count: number, foreach: (item: any, index: number) => void = null) {
            for (let i = 0; i < count; i++) {
                let instance = new cls();
                foreach && foreach(instance, i);
                instance['_itemID'] = key;
                Laya.Pool.recover(key, instance);
            }
        }

        //从对象池获取一个对象
        public static Spawn<T>(key: string): T {
            return Laya.Pool.getItem(key) as T;
        }

        //回收一个对象
        public static Despawn(obj) {
            obj.parent && obj.removeSelf();
            obj['_itemID'] && Laya.Pool.recover(obj['_itemID'], obj);
        }
    }
}