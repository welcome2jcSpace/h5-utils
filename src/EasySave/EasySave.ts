module es {
    /*
 * @Description: 数据存储类
 *
 * 将 window.localStorage 和 cookie 揉在一起 兼容不同ie版本
 *
 * 增：EasySave.set(key,value)
 * 删：EasySave.del(key)
 * 改：EasySave.set(key,value)
 * 查：EasySave.has(key)
 * 
 */
    export class EasySave {
        /**
         * 存入一个对象
         * @param key
         * @param obj
         */
        public static setObj(key: string, obj: string | object) {
            if (typeof obj == "string") {
                EasySave.set(key, obj);
            } else {
                try {
                    EasySave.set(key, JSON.stringify(obj));
                } catch (e) {
                    console.error(e);
                }
            }
        }

        /**
         * 获取一个对象
         * @param key
         * @returns null | T
         */
        public static getObj<T>(key: string): T {
            try {
                let s = EasySave.get(key);
                if (s != null && s != "") {
                    return JSON.parse(s) as T;
                }
                return null;
            } catch (e) {
                console.error(e);
                return null;
            }
        }

        /**
         * 存储Cookie
         * @param k 字符串
         * @param v 字符串
         * @param exdays 数据有效日期 默认30天, 仅Cookie有效
         */
        public static set(k: string, v: string, exdays = 30): void {
            if (window.localStorage) {
                window.localStorage.setItem(k, v);
            } else {
                var d = new Date();
                d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
                let cookie = k + "=" + v + ";expires=" + d.toUTCString();
                document.cookie = cookie;
            }
        }

        /**
         * 获取Cookie
         * @param k 字符串
         * @returns value | null
         */
        public static get(k: string): string | null {
            if (window.localStorage) {
                return window.localStorage.getItem(k);
            } else {
                var name = k + "=";
                var ca = document.cookie.split(";");
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i].trim();
                    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
                }
            }
            return null;
        }

        /**
         * 判断cookie是否存在
         * @param k
         * @returns true | false
         */
        public static has(k: string): boolean {
            if (window.localStorage) {
                return window.localStorage.getItem(k) != null;
            }

            return EasySave.get(k) != null;
        }

        /**
         * 删除cookie
         * @param k
         */
        public static del(k: string): void {
            if (window.localStorage) {
                window.localStorage.removeItem(k);
            } else {
                EasySave.set(k, "", -1);
            }
        }

        /**
         * 清理所有Cookie
         */
        public static clear() {
            if (window.localStorage) {
                window.localStorage.clear();
            } else {
                var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
                if (keys) {
                    for (var i = keys.length; i--;) {
                        document.cookie =
                            keys[i] + "=0;path=/;expires=" + new Date(0).toUTCString(); //清除当前域名下的,例如：m.kevis.com
                        document.cookie =
                            keys[i] +
                            "=0;path=/;domain=" +
                            document.domain +
                            ";expires=" +
                            new Date(0).toUTCString(); //清除当前域名下的，例如 .m.kevis.com
                        document.cookie =
                            keys[i] +
                            "=0;path=/;domain=kevis.com;expires=" +
                            new Date(0).toUTCString(); //清除一级域名下的或指定的，例如 .kevis.com
                    }
                }
            }
        }
    }
}