module es {
    export abstract class IMono {

        protected enableGUI() {
            this.disableGUI();
            es.Debug.debug && Laya.timer.loop(1, this, this.OnGUI);
        }
        protected disableGUI() {
            es.Debug.debug && Laya.timer.clear(this, this.OnGUI);
        }
        abstract OnGUI(): void;

        protected Print(any: string) {
            es.Debug.log(any);
        }
    }
}